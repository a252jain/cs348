import React from "react";
import { Typography, Select, MenuItem, Box, Button, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, IconButton } from '@mui/material';
import Searchbar from "../Searchbar";
import { useState } from "react";
import "./Follow.css";


function SearchFollower(props) {
    const userList = props.users
    const [searchValue, setSearchValue] = useState("")

    return (
        <div className="follow">
            <h3>Search by Username</h3>
            <div style={{display: 'flex',justifyContent:'space-evenly'}}>
                <Searchbar placeholder={'Username'} onChange={(event) => {

                    setSearchValue(event.target.value)
                }}/>
                <Button className='search-button' sx={{height:'55px', backgroundColor:'rgb(234, 124, 81)'}} variant='contained' onClick={() => props.search(searchValue)}>
                            Search
                    </Button>
            </div>

            <Table>
                {userList && userList.length !== 0 &&
                    <TableHead>
                        <TableRow>
                            <TableCell></TableCell>
                            <TableCell></TableCell>
                        </TableRow>
                    </TableHead>
                }
                <TableBody>
                    {  userList &&
                        userList.map((value, key) => {
                            return (
                                <TableRow key={key}>
                                    <TableCell>{value.username}</TableCell>
                                    <TableCell>
                                        <Button onClick={() => props.click(value)} disabled={!props.shouldFollow(value)}>
                                            {
                                                props.shouldFollow(value) ?
                                                'Follow' :
                                                'Followed'
                                            }
                                        </Button>
                                    </TableCell>
                                </TableRow>
                            )
                        })
                    }
                </TableBody>
            </Table>

        </div>
        
    )
}

export default SearchFollower;