import React from "react";
import { Typography, Select, MenuItem, Box, Button, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, IconButton } from '@mui/material';
import "./Follow.css";

function Follower(props) {
    const userList = props.users

    return (
        <div className="follow">
            <h3>Followers</h3>
            <Table>
                            <TableHead className='table-head'>
                                <TableRow>
                                    {/* <TableCell><b>Username</b></TableCell> */}
                                    <TableCell></TableCell>
                                    <TableCell></TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {
                                    userList.map((value, key) => {
                                        return (
                                            <TableRow key={key}>
                                                <TableCell>{value.username}</TableCell>
                                                <TableCell>
                                                    <Button onClick={() => props.click(value)} disabled={!props.shouldFollow(value)}>
                                                        {
                                                            props.shouldFollow(value) ?
                                                            'Follow back' :
                                                            'Followed'
                                                        }
                                                    </Button>
                                                </TableCell>
                                            </TableRow>
                                        )
                                    })
                                }
                                {
                                    (userList.length == 0) && 
                                    <div className="empty-followers">
                                        <h4>{"You have no followers yet... :("}</h4>
                                    </div>
                                }
                            </TableBody>
                        </Table>
        </div>
        

    )
}

export default Follower;
// onClick = {() => props.click(value)}