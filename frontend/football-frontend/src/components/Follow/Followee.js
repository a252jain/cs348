import React from "react";
import { Typography, Select, MenuItem, Box, Button, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, IconButton, Card } from '@mui/material';
import "./Follow.css";
import ClearIcon from '@mui/icons-material/Clear';

function Followee(props) {
    const userList = props.users

    return (
        <div className="follow">
            <h3>Following</h3>
            <Table>
                <TableHead className='table-head'>
                    <TableRow>
                        {/* <TableCell><b>Username</b></TableCell> */}
                        <TableCell></TableCell>
                        <TableCell></TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {
                        userList.map((value, key) => {
                            return (
                                <TableRow key={key}>
                                    <TableCell>{value.username}</TableCell>
                                    <TableCell>
                                        <ClearIcon onClick = {() => props.click(value)} />
                                    </TableCell>
                                </TableRow>
                            )
                        })
                    }
                    {
                        (userList.length === 0) && 
                        <div className='empty-followers'>
                            <h4>Oops, you're not following anyone...</h4>
                        </div>
                    }
                </TableBody>
            </Table>

        </div>
        
    )
}

export default Followee;