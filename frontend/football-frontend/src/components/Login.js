import React from 'react'
import "./Login.css"
import { useState } from 'react';
import { login } from "../requests";
import TextField from '@mui/material/TextField';
import { Button, Box } from '@mui/material';
import { useNavigate } from 'react-router-dom';

function Login() {
  const [username, setUsername] = useState();
  const [password, setPassword] = useState();
  const navigate = useNavigate();

  const handleSubmit = async e => {
    e.preventDefault();
    console.log("logging in...")
    const loginResult = await login(username, password);
    console.log("Login result:" + loginResult);
    if (loginResult.session_id !== undefined) {
      navigate('/');
    }
  }
  return (
    <div className="login-container">
      <h1>Welcome Back!</h1>
      <form onSubmit={handleSubmit}>
        <Box sx={{my: 2, display: 'flex', flexDirection: 'column', alignItems: 'center'}}>
            <TextField label="Username" required onChange={e => setUsername(e.target.value)} sx={{my: 1, fontFamily:'Open Sans'}}/>
            <TextField label="Password" required type="password" onChange={e => setPassword(e.target.value)} sx={{my: 1}}/>
            <Button variant="contained" type="submit" sx={{backgroundColor:'rgb(245, 69,0)'}}>Login</Button>
        </Box>
      </form>
    </div>
    
  )
}

export default Login