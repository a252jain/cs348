import React from 'react'
import "./Signup.css"
import { useState } from 'react';
import { signup } from '../requests';
import TextField from '@mui/material/TextField';
import { Button, Box } from '@mui/material';
import { useNavigate } from 'react-router-dom';

function SignUp() {
  const [name, setName] = useState();
  const [username,  setUsername] = useState();
  const [password, setPassword] = useState();
  const navigate = useNavigate();

  const handleSubmit = async e => {
    e.preventDefault();
    try {
      console.log("Signing up...");
      const signupResult = await signup(username, password, name);
      console.log("Signup result:");
      console.log(signupResult);
      if (signupResult.session_id !== undefined) {
        navigate('/');
      }
    } catch (e) {
      console.log("Couldn't sign up");
      console.log(e);
    }
  }

  return (
    <div className='signup-container'>
      <h1>Don't have an account?</h1>
      <form onSubmit={handleSubmit}>
        <Box sx={{my: 'auto', display: 'flex', flexDirection: 'column', alignItems: 'center'}}>
            <TextField label="Name" required onChange={e => setName(e.target.value)} sx={{my: 1}}/>
            <TextField label="Username" required onChange={e => setUsername(e.target.value)} sx={{my: 1}}/>
            <TextField label="Password" required type="password" onChange={e => setPassword(e.target.value)} sx={{my: 1}}/>
            <Button variant="contained" type="submit" sx={{backgroundColor:'rgb(245, 69,0)'}}>Sign Up</Button>
        </Box>
      </form>
    </div>
  )
}

export default SignUp