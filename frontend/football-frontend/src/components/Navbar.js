import React from 'react';
import './Navbar.css';
import { useState } from 'react';
import { Button } from '@mui/material';
import { useNavigate } from 'react-router-dom';
import { Typography, Box, AppBar } from '@mui/material';
import AccountCircle from '@mui/icons-material/AccountCircle';
import {validate} from '../requests';

function Navbar(props) {
    const [loggedIn, setLoggedIn] = useState(false, []);

    validate(!props.allowRedirect).then((validated) => {
      if (typeof validated.error === 'undefined') {
        setLoggedIn(true);
        if (!props.allowRedirect) navigate('/profile');
      } 
    });
    
    const navigate = useNavigate();
    const handleLoginClick = e => {
      navigate("/login-signup");
    }
    const handleProfileClick = e => {
      navigate("/profile");
    }
    const handleTitleClick = e => {
      navigate("/");
    }
    const handleCTClick = e => {
      navigate("/teambuilder");
    }
    const handleFollowClick = e => {
      navigate("/follow")
    }
    const handleFavouriteClick = e => {
      navigate("/favourite")
    }

    return (
      <div>
        <AppBar position='static' className='navbar-container' elevation={0}>
          <Box sx={{display: 'flex', marginLeft: 0}} className='navbar-container'>
            <Typography variant='h4' sx={{cursor:'pointer', flexGrow: 1, mb: -5, ml: 1, my: 1, marginLeft: '30px', fontFamily:'Qatar-2022', fontWeight:'bold'}} onClick={handleTitleClick}>European Soccer League Database</Typography>
            { !loggedIn &&
            <Box sx={{display: 'flex', flexDirection:'column', justifyContent:'center'}}>
              <Button onClick={handleLoginClick} sx={{color:'white', fontFamily:'Poppins'}}>{loggedIn ? "logged in" : "login"}</Button>
            </Box>
            }

            { loggedIn &&
            <Box sx={{display: 'flex', flexDirection:'column', justifyContent:'center', marginRight: 5}}>
              <Button className='navbar-buttons' onClick={handleCTClick} sx={{color:'white'}}>{"Make Custom Team"}</Button>
            </Box>
            }

            { loggedIn &&
            <Box sx={{display: 'flex', flexDirection:'column', justifyContent:'center', marginRight: 5}}>
              <Button className='navbar-buttons' onClick={handleFavouriteClick} sx={{color:'white'}}>{"Favourite Teams"}</Button>
            </Box>
            }

            { loggedIn &&
            <Box sx={{display: 'flex', flexDirection:'column', justifyContent:'center', marginRight: 5}}>
              <Button className='navbar-buttons' onClick={handleFollowClick} sx={{color:'white'}}>{"Follow"}</Button>
            </Box>
            }

            { loggedIn &&
            <Box sx={{display: 'flex', flexDirection:'column', justifyContent:'center', marginRight: 5}}>
              <AccountCircle className='profile-icon' onClick={handleProfileClick}/>
            </Box>
            }
          </Box>
      </AppBar>
    </div>
  )
}

export default Navbar;