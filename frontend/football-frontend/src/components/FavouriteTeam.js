import React, { useEffect } from 'react'
import { Select, MenuItem, Box, Button, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, IconButton } from '@mui/material';
import { playerSearchFun, teamSearchFun, matchSearchFun, addfavorite, removefavorite } from '../requests.js'
import { StarBorder, Star, Info } from '@mui/icons-material';
import {validate, getfavorite, getfavoriteids} from '../requests';
import { useState } from 'react';

function FavouriteTeam() {
    const [favoriteTeams, setFavoriteTeams] = useState([]);
    // set non-existent id in case of weird interaction
    const [userId, setUserId] = useState(-1);

    useEffect(() => {
        validate().then((validated) => {
            if (validated.user_id) {
                setUserId(validated.user_id);
                getfavorite(validated.user_id).then((result) => {
                    const teamList = result.favorites;
                    setFavoriteTeams(teamList);
                });
            }
        });
    },[]);

    async function removeFavorite(user_id, team_id) {
        await removefavorite(user_id, team_id);
    }

    return (
        <Table>
                            <TableHead>
                                <TableRow className = "header">
                                    <TableCell><b>Team Name</b></TableCell>
                                    <TableCell></TableCell>
                                    <TableCell><b>League Name</b></TableCell>
                                    <TableCell><b>Country</b></TableCell>
                                    <TableCell><b>Build Up Play Dribbling</b></TableCell>
                                    <TableCell><b>Build Up Play Passing</b></TableCell>
                                    <TableCell><b>Build Up Play Speed</b></TableCell>
                                    <TableCell></TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {
                                    favoriteTeams.map((value, key) => {
                                        return (
                                            <TableRow key={key}>
                                                <TableCell>{value.long_name}</TableCell>
                                                <TableCell>{value.short_name}</TableCell>
                                                <TableCell>{value.league_name}</TableCell>
                                                <TableCell>{value.country}</TableCell>
                                                <TableCell>{value.build_up_play_dribbling}</TableCell>
                                                <TableCell>{value.build_up_play_passing}</TableCell>
                                                <TableCell>{value.build_up_play_speed}</TableCell>
                                                <TableCell>
                                                        <IconButton name="details" onClick={(e) => {
                                                            const newList = favoriteTeams.filter((team) => {
                                                                return team.id !== value.id;
                                                            })
                                                            setFavoriteTeams(newList);
                                                            removeFavorite(userId, value.id);
                                                        }} >
                                                        <Star color = 'secondary' />
                                                    </IconButton>
                                                </TableCell>
                                            </TableRow>
                                        )
                                    })
                                }
                            </TableBody>
                        </Table>

    )
}

export default FavouriteTeam