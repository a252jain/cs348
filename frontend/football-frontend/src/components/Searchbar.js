import React from 'react'
import './Searchbar.css'
import TextField from '@mui/material/TextField';
import Search from '@mui/icons-material/Search';
import InputAdornment from '@mui/material/InputAdornment';

function Searchbar(props) {
  return (
    <div className='searchBarContainer' >
        <TextField sx={{width:1, backgroundColor:'white'}} type="text" InputProps={{endAdornment: (<InputAdornment position="end"><Search /></InputAdornment>), }} placeholder={props.type} onChange = {props.onChange}/>
    </div>
  )
}

export default Searchbar