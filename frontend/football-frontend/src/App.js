import './App.css';
import { Routes, Route } from 'react-router-dom';
import Profile from './routes/Profile';
import Follow from './routes/Follow';
import Home from './routes/Home';
import { BrowserRouter } from 'react-router-dom';
import LoginSignup from './routes/LoginSignup';
import Favourite from './routes/Favourite';
import CustomTeam from './routes/CustomTeam';

function App() {

  return (
    <div>
      <BrowserRouter>
        <Routes>
          <Route exact path="/" element={<Home />}></Route>
          <Route path="/teambuilder" element={<CustomTeam />}></Route>
          <Route path="/profile" element={<Profile />}></Route>
          <Route path="/login-signup" element={<LoginSignup />}></Route>
          <Route path="/follow" element={<Follow />}></Route>
          <Route path="/favourite" element={<Favourite />}></Route>
        </Routes>
      </BrowserRouter>
      
    </div>
  );
}

export default App;
