import React, { useEffect, useRef } from 'react'
import Searchbar from '../components/Searchbar';
import { useState } from 'react';
import Navbar from '../components/Navbar';
import { Select, MenuItem, Box, Button, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, IconButton } from '@mui/material';
import './Home.css'
import { playerSearchFun, teamSearchFun, matchSearchFun, addfavorite, removefavorite } from '../requests.js'
import { StarBorder, Star, Info } from '@mui/icons-material';
import {validate, getfavorite, getfavoriteids} from '../requests';



function Home() {
    const bottomRef = useRef(null);

    const [pageNumber, setPageNumber] = useState(0);
    const [showResults, setShowResults] = useState(false);
    const [searchResults, setSearchResults] = useState();
    const [searchType, setSearchType] = useState(0); // 0:player, 1:team, 2:match, update whenever search button is pressed

    const [playerSearchIsShown, setPSIsShown] = useState(true);
    const [teamSearchIsShown, setTSIsShown] = useState(false);
    const [matchSearchIsShown, setMSIsShown] = useState(false);

    const [playerSearch, setPlayerSearch] = useState('');
    const [playerSearchTeamFilter, setPlayerSearchTeamFilter] = useState('');
    const [playerSearchRatingFilter, setPlayerSearchRatingFilter] = useState('');
    const [playerSearchPotentialFilter, setPlayerSearchPotentialFilter] = useState('');

    const [teamSearch, setTeamSearch] = useState('');
    const [teamSearchLeagueFilter, setTeamSearchLeagueFilter] = useState('');
    const [teamSearchSpeedFilter, setTeamSearchSpeedFilter] = useState('');
    const [teamSearchPassingFilter, setTeamSearchPassingFilter] = useState('');
    const [teamSearchDribblingFilter, setTeamSearchDribblingFilter] = useState('');

    const [matchSearchLeagueFilter, setMatchSearchLeagueFilter] = useState('');
    const [matchSearchHomeTeamFilter, setMatchSearchHomeTeamFilter] = useState('');
    const [matchSearchAwayTeamFilter, setMatchSearchAwayTeamFilter] = useState('');
    const [matchSearchHomeGoalFilter, setMatchSearchHomeGoalFilter] = useState('');
    const [matchSearchAwayGoalFilter, setMatchSearchAwayGoalFilter] = useState('');

    const [favouriteList, setFavouriteList] = useState([]);
    const [loggedIn, setLoggedIn] = useState(false);
    const [userId, setUserId] = useState();

    useEffect(() => {
        validate().then((validated) => {
            if (validated.user_id) {
                setLoggedIn(true);
                setUserId(validated.user_id);
                getfavoriteids(validated.user_id).then((result) => {
                    const idList = result.favorites.map((obj) => obj.team_id);
                    setFavouriteList(idList);
                });
            }
        });
    },[]);

    const handleSearchSelect = (e) => {
        setPageNumber(0);
        switch (e.target.value) {
            case 0:
                setTSIsShown(false);
                setMSIsShown(false);
                setPSIsShown(current => !current);
                break;
            case 1:
                setPSIsShown(false);
                setMSIsShown(false);
                setTSIsShown(current => !current);
                break;
            case 2:
                setPSIsShown(false);
                setTSIsShown(false);
                setMSIsShown(current => !current);
                break;
            default:
                break;
        }
    }

    const incrementPage = () => {
        setPageNumber(pageNumber + 1);
        console.log(pageNumber);
        if (playerSearchIsShown) {
            handlePlayerSearch();
        } else if (teamSearchIsShown) {
            handleTeamSearch();
        } else if (matchSearchIsShown) {
            handleMatchSearch();
        }
    }
    const decrementPage = () => { 
        if (pageNumber >= 1) {
            setPageNumber(pageNumber - 1);
        }
        console.log(pageNumber);
        if (playerSearchIsShown) {
            handlePlayerSearch();
        } else if (teamSearchIsShown) {
            handleTeamSearch();
        } else if (matchSearchIsShown) {
            handleMatchSearch();
        }
    }

    async function handlePlayerSearch() {
        const a = await playerSearchFun(playerSearch, playerSearchTeamFilter, playerSearchRatingFilter, playerSearchPotentialFilter, pageNumber, 10);
        console.log("a:");
        console.log(a);
        setSearchResults([...a["players"]]);
        console.log("Search results: ");
        console.log(searchResults); 
        setShowResults(true);
        setSearchType(0);
        bottomRef.current?.scrollIntoView({behavior: 'smooth'});
    }

    async function handleTeamSearch() {
        const a = await teamSearchFun(teamSearch, teamSearchLeagueFilter, teamSearchSpeedFilter, teamSearchPassingFilter, teamSearchDribblingFilter, pageNumber, 10);
        setSearchResults([...a["teams"]])
        setShowResults(true);
        setSearchType(1);
        bottomRef.current?.scrollIntoView({behavior: 'smooth'});
    }

    async function handleMatchSearch() {
        const a = await matchSearchFun(matchSearchLeagueFilter, matchSearchHomeTeamFilter, matchSearchAwayTeamFilter, matchSearchHomeGoalFilter, matchSearchAwayGoalFilter, pageNumber, 10);
        setSearchResults([...a["matches"]])
        setShowResults(true);
        setSearchType(2);
        bottomRef.current?.scrollIntoView({behavior: 'smooth'});
    }

    async function addFavorite(user_id, team_id) {
        await addfavorite(user_id, team_id);
    }

    async function removeFavorite(user_id, team_id) {
        await removefavorite(user_id, team_id);
    }

    async function favoriteOnClick(team_id) {
            if (favouriteList.includes(team_id)) {
                const newList = favouriteList.filter((item) => {
                    return item != team_id;
                })
                setFavouriteList(newList);
                removeFavorite(userId, team_id);
            } else {
                setFavouriteList([...favouriteList, team_id])
                addFavorite(userId, team_id);
            }
    }

    return (
        <div>
            <Navbar allowRedirect={true} />
        <div className="App">
            
            <Box sx={{display:'flex', flexDirection:'column', justifyContent:'center', padding: 15}}>
                <Box className='select-search-container' sx={{display:'flex', flexDirection:'row', justifyContent:'center', fontFamily: 'Open Sans Condensed'}}>
                    <Select defaultValue={0} onChange={handleSearchSelect} sx={{backgroundColor: 'rgb(245, 69,0)', color:'white', height:1, fontFamily:'Open Sans', fontWeight:'bold'}}>
                        <MenuItem value={0}>Player Search</MenuItem>
                        <MenuItem value={1}>Team Search</MenuItem>
                        <MenuItem value={2}>Match Search</MenuItem>
                    </Select>
                
                {playerSearchIsShown && 
                <div>
                    <div className="main-search">
                        <Searchbar type="Player Search" onChange={(event) => {
                            console.log(event);
                            setPlayerSearch(event.target.value)
                            setPageNumber(0);
                        }}
                        />
                    </div>
                    <div className="filter">
                        <Searchbar type="Team" onChange={(event) => {
                            setPlayerSearchTeamFilter(event.target.value)
                            setPageNumber(0)
                        }}
                        />
                        <Searchbar type="Rating" onChange={(event) => {
                            setPlayerSearchRatingFilter(event.target.value)
                            setPageNumber(0)
                        }}
                        />
                        <Searchbar type="Potential" onChange={(event) => {
                            setPlayerSearchPotentialFilter(event.target.value)
                            setPageNumber(0)
                        }}
                        />
                    </div>
                </div>}

                    
                {teamSearchIsShown && 
                <div>
                    <div className="main-search">
                        <Searchbar type="Team Search" onChange={(event) => {
                                setTeamSearch(event.target.value)
                                setPageNumber(0);
                            }}
                            />
                    </div>
                    <div className="filter">
                        <Searchbar type="League" onChange={(event) => {
                                setTeamSearchLeagueFilter(event.target.value)
                                setPageNumber(0);
                            }}
                            />
                        <Searchbar type="Build Up Speed" onChange={(event) => {
                                setTeamSearchSpeedFilter(event.target.value);
                                setPageNumber(0);
                            }}
                            />
                        <Searchbar type="Build Up Passing" onChange={(event) => {
                                setTeamSearchPassingFilter(event.target.value);
                                setPageNumber(0);
                            }}
                            />
                        <Searchbar type="Build Up Dribbling" onChange={(event) => {
                                setTeamSearchDribblingFilter(event.target.value)
                                setPageNumber(0);
                            }}
                            />
                    </div>
                    
                </div>}

                {matchSearchIsShown && 
                <div>
                    <div className="filter">
                        <Searchbar type="League" onChange={(event) => {
                                setMatchSearchLeagueFilter(event.target.value)
                                setPageNumber(0);
                            }}
                            />
                        <Searchbar type="Home Team" onChange={(event) => {
                                setMatchSearchHomeTeamFilter(event.target.value)
                                setPageNumber(0);
                            }}
                            />
                        <Searchbar type="Away Team" onChange={(event) => {
                                setMatchSearchAwayTeamFilter(event.target.value)
                                setPageNumber(0);
                            }}
                            /> 
                        <Searchbar type="Home Goals" onChange={(event) => {
                                setMatchSearchHomeGoalFilter(event.target.value)
                                setPageNumber(0);
                            }}
                            />
                        <Searchbar type="Away Goals" onChange={(event) => {
                                setMatchSearchAwayGoalFilter(event.target.value)
                                setPageNumber(0);
                            }}
                            />
                    </div>
                </div>}
                <div style={{height:'auto'}} className='search-button-container'>
                {playerSearchIsShown && 
                    <Button className='search-button' sx={{height:'50%', backgroundColor: 'rgb(245, 69,0)'}} variant='contained' onClick={handlePlayerSearch}>
                            Search
                    </Button>
                }
                {teamSearchIsShown && 
                    <Button className='search-button' sx={{height:'50%', backgroundColor: 'rgb(245, 69,0)'}} variant='contained' onClick={handleTeamSearch}>
                        Search
                    </Button>
                }
                {matchSearchIsShown && 
                    <Button className='search-button' sx={{height:'100%', backgroundColor: 'rgb(245, 69,0)'}} variant='contained' onClick={handleMatchSearch}>
                            Search
                    </Button>
                }
                </div>

                </Box>
            </Box>
            {showResults && 
            <Box>
                <Box sx={{display:'flex', flexDirection:'column'}}>
                    <Box sx={{display:'flex', flexDirection:'row', justifyContent:'space-around', mx:1}}>
                        <Button className='page-button' variant='outlined' onClick={decrementPage} sx={{color:'rgb(245, 69,0)', borderColor: 'rgb(245, 69,0)'}}>Prev</Button>
                        <h2>Results</h2>
                        <Button className='page-button' variant='outlined' onClick={incrementPage} sx={{color:'rgb(245, 69,0)', borderColor: 'rgb(245, 69,0)'}}>Next</Button>
                    </Box>
                </Box>
                <Box className="results" sx={{padding:2}}>
                    {
                    <TableContainer>
                        {(searchType === 0) &&  
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell className='table-header'>Name</TableCell>
                                    <TableCell className='table-header'>Overall Rating</TableCell>
                                    <TableCell className='table-header'>Potential</TableCell>
                                    <TableCell className='table-header'>Team</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {
                                    searchResults.map((value, key) => {
                                        return (
                                            <TableRow key={key}>
                                                <TableCell>{value.name}</TableCell>
                                                <TableCell>{value.overall_rating}</TableCell>
                                                <TableCell>{value.potential}</TableCell>
                                                <TableCell>{value.team_name}</TableCell>
                                            </TableRow>
                                        )
                                    })
                                }
                            </TableBody>
                        </Table>
                        }
                        {(searchType === 1) && 
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell className='table-header'>Team Name</TableCell>
                                    <TableCell className='table-header'></TableCell>
                                    <TableCell className='table-header'>League Name</TableCell>
                                    <TableCell className='table-header'>Country</TableCell>
                                    <TableCell className='table-header'>Build Up Play Dribbling</TableCell>
                                    <TableCell className='table-header'>Build Up Play Passing</TableCell>
                                    <TableCell className='table-header'>Build Up Play Speed</TableCell>
                                    <TableCell></TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {
                                    searchResults.map((value, key) => {
                                        return (
                                            <TableRow key={key}>
                                                <TableCell>{value.long_name}</TableCell>
                                                <TableCell>{value.short_name}</TableCell>
                                                <TableCell>{value.league_name}</TableCell>
                                                <TableCell>{value.country}</TableCell>
                                                <TableCell>{value.build_up_play_dribbling}</TableCell>
                                                <TableCell>{value.build_up_play_passing}</TableCell>
                                                <TableCell>{value.build_up_play_speed}</TableCell>
                                                {
                                                    loggedIn &&
                                                    <TableCell>
                                                         <IconButton name="details" onClick={(e) => favoriteOnClick(value.id)} >
                                                            {
                                                                favouriteList.includes(value.id) ?
                                                                    <Star color = 'secondary' />
                                                                    : <StarBorder />
                                                            }
                                                            
                                                        </IconButton>
                                                    </TableCell>
                                                }
                                            </TableRow>
                                        )
                                    })
                                }
                            </TableBody>
                        </Table>
                        }
                        {(searchType === 2) && 
                            <Table>
                                <TableHead>
                                    <TableRow>
                                        <TableCell className='table-header'>Season</TableCell>
                                        <TableCell className='table-header'>Home</TableCell>
                                        <TableCell className='table-header'>Score</TableCell>
                                        <TableCell className='table-header'>Away</TableCell>
                                        <TableCell className='table-header'>Score</TableCell>
                                        <TableCell className='table-header'>Date</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                {
                                    searchResults.map((value, key) => {
                                        return (
                                            <TableRow key={key}>
                                                <TableCell>{value.season}</TableCell>
                                                <TableCell>{value.home_team_name}</TableCell>
                                                <TableCell>{value.home_team_goal}</TableCell>
                                                <TableCell>{value.away_team_name}</TableCell>
                                                <TableCell>{value.away_team_goal}</TableCell>
                                                <TableCell>{value.date}</TableCell>
                                            </TableRow>
                                        )
                                    })
                                }
                            </TableBody>
                            </Table>
                        }
                    </TableContainer>
                    }
                </Box>
            </Box>
            
            }
            <div className="bottom-of-page" ref={bottomRef}></div>
        </div>
        </div>
  );
}

export default Home