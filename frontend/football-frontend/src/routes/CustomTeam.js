import React, { useEffect } from 'react'
import Searchbar from '../components/Searchbar';
import { useState } from 'react';
import Navbar from '../components/Navbar';
import { Select, MenuItem, Box, Button, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, IconButton, TextField, InputLabel} from '@mui/material';
import { playerSearchFun, teamSearchFun, matchSearchFun, addfavorite, removefavorite, getLeagues, addteam, addplayertoteam, getteam } from '../requests.js'
import { StarBorder, Star, Info } from '@mui/icons-material';
import {validate, getfavorite, getfavoriteids} from '../requests';
import Icon from '@mui/material/Icon';
import CancelIcon from '@mui/icons-material/Cancel';
import Card from '@mui/material/Card'
import FormControl from '@mui/material/FormControl'
import { AddCircle } from '@mui/icons-material';
import './CustomTeam.css'

function CustomTeam(areThereProps=false, props={}) {
    const [pageNumber, setPageNumber] = useState(0);
    const [showResults, setShowResults] = useState(false);
    const [searchResults, setSearchResults] = useState([]);
    const [team, setTeam] = useState([]);
    const [leagues, setLeagues] = useState([]);
    const [league, setLeague] = useState(areThereProps ? props.league : '');
    const [teamName, setTeamName] = useState(areThereProps ? props.name : '');
    const [teamScore, setTeamScore] = useState(areThereProps ? props.score : '');

    const [playerSearch, setPlayerSearch] = useState('');
    const [playerSearchTeamFilter, setPlayerSearchTeamFilter] = useState('');
    const [playerSearchRatingFilter, setPlayerSearchRatingFilter] = useState('');
    const [playerSearchPotentialFilter, setPlayerSearchPotentialFilter] = useState('');

    async function handlePlayerSearch() {
        const a = await playerSearchFun(playerSearch, playerSearchTeamFilter, playerSearchRatingFilter, playerSearchPotentialFilter, pageNumber, 10);
        console.log("a:");
        console.log(a);
        setSearchResults(a.players);
        console.log("Search results: ");
        console.log(searchResults); 
        // setShowResults(true);
    }

    const handleLeagueChange = (event) => {
        setLeague(event.target.value);
    };

    useEffect(() => {
        getLeagues().then((r) => {
            console.log(leagues, r);
            setLeagues(r)
            console.log(leagues);
        });
        // if (areThereProps) {
        //     validate(true).then((r) => {
        //         const user_id = r.user_id;
        //         getteam(props.team_id, user_id).then((r) => {
        //             setTeam(r);
        //         });
        //     });
        // }
    },[]);

    const incrementPage = () => {
        setPageNumber(pageNumber + 1);
        handlePlayerSearch();
    }
    const decrementPage = () => { 
        if (pageNumber > 0) setPageNumber(pageNumber - 1);
        handlePlayerSearch();
    }
    const addPlayerToTeam = (player) => {
        setTeam([...team, player]);
    }
    const rmPlayerToTeam = (player) => {
        const newTeam = team.filter((p) => {
            return p.pid !== player.pid
        })
        setTeam(newTeam);
    }

    const handleSave = async () => {
        const userId = (await validate(true)).user_id;
        const nt = await addteam(teamName, teamName, league, teamScore, userId);
        console.log("ok");
        console.log(team);
        for (const p of team) {
            await addplayertoteam(p.pid, nt.team_id, userId);
        }
    }
    

    return (
        <div>
            <Navbar allowRedirect={true} />
            <Card style={{width: "80%", marginLeft: "10%", marginBottom: "5vh", marginTop: "5vh", padding: "2vh", fontFamily:'Poppins'}}>
                <FormControl style={{minWidth: 120, display:"flex", flexDirection: "row", justifyContent:'flex-start', padding:2}}>
                    <Box sx={{display:'flex', flexDirection:'row'}}>
                        <TextField id="outlined-basic" label="Team name" variant="outlined" onChange={(e) => setTeamName(e.target.value)} />
                        <TextField id="outlined-basic" label="Score" variant="outlined" onChange={(e) => setTeamScore(e.target.value)} />
                        <Box sx={{display:'flex', flexDirection:'row', mx:3}}>
                            <p>League:</p>
                            <Select
                                id="demo-simple-select"
                                value={league}
                                label="League"
                                onChange={handleLeagueChange}
                                sx={{minWidth:'100%', mx:1}}
                            >
                                {
                                    leagues.map((l) => {
                                        return (<MenuItem value={l.name}>{l.name}</MenuItem>)
                                    })
                                }
                            </Select>
                        </Box>
                        
                        <Button className='save-button' id="save" variant='contained' onClick={handleSave} sx={{height:'100%', backgroundColor: 'rgb(245, 69,0)', ml:20}}>Save</Button>
                    </Box>
                </FormControl>
                <hr />
                <Box className="results">
                    <TableContainer>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell className='table-header'>Name</TableCell>
                                    <TableCell className='table-header'>Overall Rating</TableCell>
                                    <TableCell className='table-header'>Potential</TableCell>
                                    <TableCell className='table-header'>Team</TableCell>
                                    <TableCell className='table-header'></TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {   team.length > 0 &&
                                    team.map((value, key) => {
                                        return (
                                            <TableRow key={key}>
                                                
                                                <TableCell>{value.name}</TableCell>
                                                <TableCell>{value.overall_rating}</TableCell>
                                                <TableCell>{value.potential}</TableCell>
                                                <TableCell>{value.team_name}</TableCell>
                                                <TableCell>
                                                    <IconButton 
                                                        onClick={() => rmPlayerToTeam(value)}
                                                    >
                                                        <CancelIcon></CancelIcon>
                                                    </IconButton>
                                                </TableCell>
                                            </TableRow>
                                        )
                                    })
                                }
                            </TableBody>
                        </Table>
                    </TableContainer>
                </Box>
            </Card>
            <Card style={{width: "80%", marginLeft: "10%", padding: "2vh"}}>
                <Box sx={{padding:2}}><h1>Add Players</h1></Box>
                <Box className='search-filter-container' sx={{display:'flex', flexDirection:'row', padding:2}}>
                    <Box className="main-search" sx={{display:'flex', flexDirection:'column', justifyContent:'center'}}>
                        <Searchbar type="Player Search" onChange={(event) => {
                            console.log(event);
                            setPlayerSearch(event.target.value)
                            setPageNumber(0);
                        }}
                        />
                        <div className="filter">
                            <Searchbar type="Team" onChange={(event) => {
                                setPlayerSearchTeamFilter(event.target.value)
                                setPageNumber(0)
                            }}
                            />
                            <Searchbar type="Rating" onChange={(event) => {
                                setPlayerSearchRatingFilter(event.target.value)
                                setPageNumber(0)
                            }}
                            />
                            <Searchbar type="Potential" onChange={(event) => {
                                setPlayerSearchPotentialFilter(event.target.value)
                                setPageNumber(0)
                            }}
                            />
                        </div>
                    </Box>
                    
                    <Button className='search-button' sx={{height:'50%', backgroundColor: 'rgb(245, 69,0)'}} variant='contained' onClick={handlePlayerSearch}>
                            Search
                    </Button>
                </Box>
                <hr />
                <Box sx={{padding:2}}>
                    <Box sx={{display:'flex', flexDirection:'column', justifyContent:'center'}}>
                        <Box sx={{display:'flex', flexDirection:'row', justifyContent:'space-around', mx:1}}>
                            <Button className='page-button' variant='outlined' onClick={decrementPage} sx={{color:'rgb(245, 69,0)', borderColor: 'rgb(245, 69,0)'}}>Prev</Button>
                            <h2>Results</h2>
                            <Button className='page-button' variant='outlined' onClick={incrementPage} sx={{color:'rgb(245, 69,0)', borderColor: 'rgb(245, 69,0)'}}>Next</Button>
                        </Box>
                    </Box>
                    <Box className="results">
                        <TableContainer>
                            <Table>
                                <TableHead>
                                    <TableRow>
                                        <TableCell className='table-header'>Name</TableCell>
                                        <TableCell className='table-header'>Overall Rating</TableCell>
                                        <TableCell className='table-header'>Potential</TableCell>
                                        <TableCell className='table-header'>Team</TableCell>
                                        <TableCell className='table-header'></TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {   searchResults.length > 0 &&
                                        searchResults.map((value, key) => {
                                            return (
                                                <TableRow key={key}>
                                                    <TableCell>{value.name}</TableCell>
                                                    <TableCell>{value.overall_rating}</TableCell>
                                                    <TableCell>{value.potential}</TableCell>
                                                    <TableCell>{value.team_name}</TableCell>
                                                    <TableCell>
                                                        <IconButton 
                                                            onClick={() => addPlayerToTeam(value)}
                                                        >
                                                           <AddCircle></AddCircle>
                                                        </IconButton>
                                                    </TableCell>
                                                </TableRow>
                                            )
                                        })
                                    }
                                </TableBody>
                            </Table>
                        </TableContainer>
                    </Box>
                </Box>
            </Card>
        </div>
    )
}

export default CustomTeam;