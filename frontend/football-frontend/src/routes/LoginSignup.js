import React from 'react'
import Login from '../components/Login'
import SignUp from '../components/SignUp'
import Navbar from '../components/Navbar'
import "./LoginSignup.css"
import { Box } from '@mui/material';

function LoginSignup() {
  return (
    <Box sx={{}}>
        <Navbar allowRedirect={false}/>
        <div className='login-signup-container'>
          <Login />
          <SignUp />
        </div>
    </Box>
  )
}

export default LoginSignup