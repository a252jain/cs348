import React from 'react'
import Navbar from '../components/Navbar';
import {Button, Avatar}  from '@mui/material';
import {validate, getfavorite, getfavoriteids, getallcustomteams} from '../requests';
import { useState, useEffect } from 'react';

function Profile() {

  const [userName, setUserName] = useState('Loading');
  const [realName, setRealName] = useState('Loading');
  const [customTeam, setCustomTeam] = useState([]);

  useEffect(() => {
    validate().then((validated) => {
      if (validated.user_id) {
          
          setUserName(validated.user_name);
          setRealName(validated.real_name);
          getallcustomteams(validated.user_id).then((result) => {
            setCustomTeam(result.team)
            console.log("teams")
            console.log(result.team)
          });
      }
    });
  }, [])

  return (
    <div className="profileContainer">
      <Navbar allowRedirect={true} />
      <div className="profileContainer">
      <div style={{display:"flex", justifyContent:"space-evenly", margin:"35px 0px"}}>
      <Avatar src="/broken-image.jpg" sx={{height:"120px", width:"120px"}}/>
      </div>
      <div style={{fontSize: 20, display:"flex", justifyContent:"space-evenly", margin:"20px 0px"}}>  
        <h4>{userName}</h4>
      </div>
      <div>
        <h4 style={{display:"flex", justifyContent:"space-evenly", margin:"20px 0px"}}>{realName}</h4>
      </div>
      <div style={{textAlign: 'center', marginTop: '50px'}}>
        <Button onClick={ () => {
              document.cookie = document.cookie = "session_id=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
              window.location.replace("/login-signup");
          }
        }><h2 style={{display:"flex", justifyContent:"space-evenly", margin:"20px 0px"}}>Sign Out</h2></Button>
      </div>
      <div style={{display:"flex", justifyContent:"space-evenly", margin:"20px 0px"}}>
      <h1>Custom Teams</h1>
      </div>

      <div style={{display:"flex", justifyContent:"space-evenly", margin:"20px 0px"}}>
        
        {customTeam.length > 0 &&
         customTeam.map((element) => {
            return (<div><p>{element.team_id}</p></div>)
         })}
        {customTeam.length === 0 && <p>No teams found</p>}
      </div>
    </div>
    </div>
  );
}

export default Profile