import React, {useEffect} from 'react'
import Navbar from '../components/Navbar';
import {validate, getFollowees, getFollowers, addfriend, removefriend, searchuser} from '../requests';
import Followee from '../components/Follow/Followee';
import Follower from '../components/Follow/Follower';
import { useState } from 'react';
import "./Follow.css"
import SearchFollower from '../components/Follow/SearchFollower';

function Follow() {

    const [userId, setUserId] = useState();
    const [followeeList, setFolloweeList] = useState([]);
    const [followerList, setFollowerList] = useState([]);
    const [searchResult, setSearchResult] = useState([]);

    useEffect(() => {
        validate().then((validated) => {
            if (validated.user_id) {
                setUserId(validated.user_id);
                getFollowees(validated.user_id).then((result) => {
                    const list = result.followees
                    setFolloweeList(list)
                })
                getFollowers(validated.user_id).then((result) => {
                    const list = result.followers
                    setFollowerList(list)
                })
                // need to get people I follow
                // need to get people following me 
            }
        });
    },[]);

    async function removeFriend(user_id, followee_id) {
        await removefriend(user_id, followee_id)
    }

    async function deleteFollowee(user) {
        const newList = followeeList.filter((followee) => {
            return followee.username !== user.username
         })
         setFolloweeList(newList)
         removeFriend(userId, user.id)
    }

    async function deleteFollower(user) {
        const newList = followerList.filter((follower) => {
            return follower.username !== user.username
        })
        setFollowerList(newList)
        removeFriend(user.id, userId)
    }

    async function addFollowee(user) {
        setFolloweeList([...followeeList, user])
        addfriend(userId, user.id);
    }

    function shouldFollow(user) {
        for (let i = 0; i < followeeList.length; i++) {
            if (user.username === followeeList[i].username || user.id === userId) {
                return false
            }
        }
        return true
    }
    
    async function searchUser(text) {
        searchuser(text).then((result) => {
            setSearchResult(result.users);
        })
    
    }

  return (
    <div className="followContainer">
      <Navbar allowRedirect={true} />
      <div className='follow-container'>
        <Followee users={followeeList} click={deleteFollowee}/>
        <Follower users={followerList} click={addFollowee} shouldFollow={shouldFollow}/>
        <SearchFollower users={searchResult} search={searchUser} shouldFollow={shouldFollow} click={addFollowee} />
      </div>

    </div>
  );
}

export default Follow