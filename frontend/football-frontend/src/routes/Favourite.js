import React from 'react'
import Navbar from '../components/Navbar';
import FavouriteTeam from '../components/FavouriteTeam';

function Favourite() {
  return (
    <div className="favouriteContainer">
      <Navbar allowRedirect={true} />
      <FavouriteTeam/>
    </div>
  );
}

export default Favourite