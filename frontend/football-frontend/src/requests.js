import axios from "axios";

const domain = "http://localhost:3001";

export async function validate(fromlogin=false) {
    const session_id = document.cookie.split('=')[1];
    console.log(fromlogin)

    const res = await axios.post(`${domain}/validate`,{
        "uuid" : session_id
    })

    const loggedIn = typeof res.data.error === 'undefined';

    if (loggedIn) {
        document.cookie = `session_id=${session_id}; expires=${new Date(new Date().setFullYear(new Date().getFullYear() + 1))}; path=/;`
        return {user_id: res.data.user_id, user_name: res.data.user_name, real_name: res.data.real_name}
    } else {
        if (!fromlogin) {
            const path = window.location.pathname;
            document.cookie = "session_id=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;"
            window.location.replace("/login-signup");
        }
        return {error: "not validated"};
    }
};

export async function login(username, password) {
    const res = await axios.post(`${domain}/login`,{
        "username" : username,
        "password" : password 
    })

    const loggedIn = typeof res.data.error === 'undefined';
    if (loggedIn) {
        document.cookie = `session_id=${res.data.session_id}`
        return {session_id: res.data.session_id};
    } else {
        return {error: res.data.error};
    }
}

export async function signup(username, password, name) {
    const res = await axios.post(`${domain}/newuser`, {
        username: username,
        password: password,
        name: name
    })

    const loggedIn = typeof res.data.error === 'undefined';
    if (loggedIn) {
        document.cookie = `session_id=${res.data.session_id}`
        return {session_id: res.data.session_id};
    } else {
        return {error: res.data.error};
    }
}

export async function getLeagues() {
    const res = await axios.get(`${domain}/leagues`, {})
    console.log(res)
    console.log(res.data.leagues[0])
    return res.data.leagues[0];
}

export async function playerSearchFun(player, team, rating, potential, page_number, page_count) {
    const params = {
        player_name: player,
        team_name: team,
        overall_rating: rating,
        potential: potential,
        page_number: page_number,
        count: page_count
    }
    const res = await axios.get(`${domain}/playersearch`, {
        params
    })
    // console.log(res.data)
    return res.data;
}

export async function teamSearchFun(team, league, speed, passing, dribbling, page_number, page_count) {
    const params = {
        team_name: team,
        league_name: league,
        build_up_play_speed: speed,
        build_up_play_passing: passing,
        build_up_play_dribbling: dribbling,
        page_number: page_number,
        count: page_count
    }
    const res = await axios.get(`${domain}/teamsearch`, {
        params
    })
    console.log(res.data)
    return res.data;

}

export async function matchSearchFun(league, home_team, away_team, home_goal, away_goal, page_number, page_count) {
    const params = {
        away_goals: away_goal,
        home_goals: home_goal,
        home_team: home_team,
        away_team: away_team,
        league_name: league,
        page_number: page_number,
        count: page_count
    }
    const res = await axios.get(`${domain}/matchsearch`, {
        params
    })
    console.log(res.data)
    return res.data;

}

export async function addfavorite(user_id, team_id) {
    const res = await axios.post(`${domain}/addfavorite`, {
        team_id: team_id,
        user_id: user_id
    })

    if (!res.data.error) {
        return res.data;
    } else {
        return {error: res.data.error};
    }
}

export async function removefavorite(user_id, team_id) {
    console.log("called");
    const data = {data: {
        team_id: team_id,
        user_id: user_id
    }};
    const res = await axios.delete(`${domain}/removefavorite`, data)

    if (!res.data.error) {
        return res.data;
    } else {
        return {error: res.data.error};
    }
}

export async function getfavorite(user_id) {
    const params = {
        user_id: user_id
    }
    const res = await axios.get(`${domain}/getfavorites`, {
        params
    })

    console.log(res);

    if (!res.data.error) {
        return {favorites: res.data.favorites};
    } else {
        return {error: res.data.error};
    }
}

export async function getfavoriteids(user_id) {
    const params = {
        user_id: user_id
    }
    const res = await axios.get(`${domain}/getfavoriteids`, {
        params
    })
    if (!res.data.error) {
        return {favorites: res.data.id};
    } else {
        return {error: res.data.error};
    }

}

export async function addfriend(requester_id, requested_id) {
    console.log('ids');
    console.log(requested_id);
    console.log(requester_id);
    const res = await axios.post(`${domain}/addfriend`, {
        requester_id: requester_id, 
        requested_id: requested_id
    })

    if (!res.data.error) {
        return res.data;
    } else {
        return {error: res.data.error};
    }
}

export async function removefriend(follower_id, followee_id) {
    const data = {data: {
        requester_id: follower_id,
        requested_id: followee_id
    }};
    const res = await axios.delete(`${domain}/removefriend`, data)

    if (!res.data.error) {
        return res.data;
    } else {
        return {error: res.data.error};
    }
}

// gets everyone followed by the user
export async function getFollowees(user_id) {
    const params = {
        user_id: user_id
    }
    const res = await axios.get(`${domain}/getfollowees`, {
        params
    })

    if (!res.data.error) {
        return {followees: res.data.followees};
    } else {
        return {error: res.data.error};
    }
}

export async function getFollowers(user_id) {
    const params = {
        user_id: user_id
    }
    const res = await axios.get(`${domain}/getfollowers`, {
        params
    })
    if (!res.data.error) {
        return {followers: res.data.followers};
    } else {
        return {error: res.data.error};
    }
}

export async function searchuser(user_name) {
    const params = {
        username: user_name
    }
    const res = await axios.get(`${domain}/usersearch`, {
        params
    })
    if (!res.data.error) {
        return {users: res.data.users};
    } else {
        return {error: res.data.error};
    }
}

export async function addteam(long_name, short_name, league_name, score, user_id) {
    const res = await axios.post(`${domain}/addteam`, {
        long_name: long_name,
        short_name: short_name,
        score: score,
        league_name: league_name,
        user_id: user_id
    })

    if (!res.data.error) {
        return res.data;
    } else {
        return {error: res.data.error};
    }
}

export async function removeteam(team_id, user_id) {
    const res = await axios.post(`${domain}/removeteam`, {
        team_id: team_id,
        user_id: user_id
    })

    if (!res.data.error) {
        return res.data;
    } else {
        return {error: res.data.error};
    }
}

export async function updateteam(long_name, short_name, team_id, score, user_id) {
    const res = await axios.post(`${domain}/updateteam`, {
        long_name: long_name,
        short_name: short_name,
        score: score,
        team_id: team_id,
        user_id: user_id
    })

    if (!res.data.error) {
        return res.data;
    } else {
        return {error: res.data.error};
    }
}

export async function getteam(team_id, user_id) {
    const res = await axios.post(`${domain}/getteam`, {
        team_id: team_id,
        user_id: user_id
    })

    if (!res.data.error) {
        return {team: res.data.team};
    } else {
        return {error: res.data.error};
    }
}

export async function addplayertoteam(player_id, team_id, user_id) {
    console.log("adding player")
    console.log(player_id);
    console.log(team_id);
    const res = await axios.post(`${domain}/addplayertoteam`, {
        player_id: player_id,
        team_id: team_id,
        user_id: user_id
    })

    if (!res.data.error) {
        return res.data;
    } else {
        return {error: res.data.error};
    }
}

export async function getallcustomteams(user_id) {
    const res = await axios.post(`${domain}/getallcustomteams`, { 
        user_id: user_id
    })

    if (!res.data.error) {
        return {team: res.data.teams};
    } else {
            return {error: res.data.error};
        }    
}