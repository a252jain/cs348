const express = require("express");

module.exports = function(db) {
    const app = express();

    app.get("/teamsearch", async (req, res) => {
        const page_number = !req.query.page_number  ? 0 : req.query.page_number;
        const count = !req.query.count ? 10 : req.query.count;
        const team_name_filter = !req.query.team_name ? 'TRUE' : `(t.long_name LIKE '%${req.query.team_name}%' OR t.short_name like '%${req.query.team_name}%')`;
        const league_name_filter = !req.query.league_name ? 'TRUE' : `t.league_name like '%${req.query.league_name}%'`;
        const play_speed_filter = !req.query.build_up_play_speed ? 'TRUE' : `build_up_play_speed >= ${req.query.build_up_play_speed}`;
        const play_passing_filter = !req.query.build_up_play_passing ? `TRUE` : `build_up_play_passing >= ${req.query.build_up_play_passing}`;
        const play_dribbling_filter = !req.query.build_up_play_dribbling ? `TRUE` : `build_up_play_dribbling >= ${req.query.build_up_play_dribbling}`;
        try {
            const teams = await db.query(
                `SELECT t.id, t.league_name, c.name as country, t.long_name, t.short_name, t.build_up_play_speed, t.build_up_play_passing, t.build_up_play_dribbling
                FROM 
                    full_existing_team t
                    LEFT JOIN league l on t.league_name = l.name
                    LEFT JOIN country c on c.id = l.country_id
                WHERE 
                    ${team_name_filter} AND
                    ${league_name_filter} AND
                    ${play_speed_filter} AND
                    ${play_passing_filter} AND
                    ${play_dribbling_filter}
                ORDER BY Build_up_play_speed DESC
                LIMIT ${page_number * count}, ${count};`
                , { nest: true }
            )
            res.status(200).send({"teams": teams});
        } catch (err) {
            res.status(200).send({"error" : "Internal Server Error"});
        }
    });

    app.get("/leagues", async(req, res) => {
        try {
            const leagues = await db.query(
                `SELECT * FROM league`
            )
            console.log(leagues)
            res.status(200).send({"leagues": leagues});
        } catch (err) {
            res.status(200).send({"error" : "Internal Server Error"});
        }
    });

    return app;
}