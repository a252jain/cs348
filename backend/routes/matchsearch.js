const express = require("express");

module.exports = function(db) {
    const app = express();

    app.get("/matchsearch", async (req, res) => {
        var page_number = !req.query.page_number ? 0 : req.query.page_number;
        var count = !req.query.count ? 10 : req.query.count;
        const away_team_goal_filter = !req.query.away_goals ? 'TRUE': `away_team_goal >= ${req.query.away_goals}`;
        const home_team_goal_filter = !req.query.home_goals ? 'TRUE': `home_team_goal >= ${req.query.home_goals}`;
        const home_team_name_filter = !req.query.home_team ? 'TRUE': `(t1.long_name LIKE '%${req.query.home_team}%' OR t1.short_name LIKE '%${req.query.home_team}%')`;
        const away_team_name_filter = !req.query.away_team ? 'TRUE': `(t2.long_name LIKE '%${req.query.away_team}%' OR t2.short_name LIKE '%${req.query.away_team}%')`;
        const league_name_filter = !req.query.league_name ? 'TRUE': `m.league_name LIKE '%${req.query.league_name}%'`;
        try {
            const matches = await db.query(
                `SELECT m.id, home_team_goal, away_team_goal , m.season, m.date, t1.long_name as home_team_name, t2.long_name as away_team_name
                FROM 
                    \`match\` m
                    LEFT JOIN team t1
                    ON t1.id = home_team_id
                    LEFT JOIN team t2
                    ON t2.id = away_team_id
                WHERE 
                    ${away_team_goal_filter} AND
                    ${home_team_goal_filter} AND
                    ${home_team_name_filter} AND
                    ${away_team_name_filter} AND
                    ${league_name_filter}
                ORDER BY date
                LIMIT ${page_number  * count}, ${count};`
                , { nest: true }
            )
            res.status(200).send({"matches": matches});
        } catch (err) {
            res.status(200).send({"error" : "Internal Server Error"});
        }
    });

    return app;
}