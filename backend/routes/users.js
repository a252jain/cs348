const express = require("express");
const crypto = require("crypto");
const { v4: uuidv4 } = require('uuid');

module.exports = function(db) {
    const app = express();

    // generates a salt using crypto's function
    // idea from https://blog.logrocket.com/building-a-password-hasher-in-node-js/
    let generateSalt = rounds => {
        if (rounds >= 15) {
            throw new Error(`${rounds} is greater than 15,Must be less that 15`);
        }
        if (typeof rounds !== 'number') {
            throw new Error('rounds param must be a number');
        }
        if (rounds == null) {
            rounds = 12;
        }
        return crypto.randomBytes(Math.ceil(rounds / 2)).toString('hex').slice(0, rounds);
    };
    
    // generate a hash based on the unhashed password and salt
    let generateHash = (password, salt) => {
        if (password == null || salt == null) {
            throw new Error('Must Provide Password and salt values');
        }
        let hash = crypto.createHmac('sha512', salt);
        hash.update(password);
        let value = hash.digest('hex');
        return value;
    };

    // compare passwords for login
    let comparePasswords = (password, hashedPassword, salt) => {
        if (password == null || hashedPassword == null || salt == null) {
            throw new Error('password and hash is required to compare');
        }
        let passwordData = generateHash(password, salt);
        if (passwordData === hashedPassword) {
            return true;
        }
        return false;
    };

    app.post("/login", async (req, res) => {
        try {
            // username is unique, so we can do this
            const users = await db.query(
                `SELECT password,salt,uuid  FROM user_profile
                WHERE username = '${req.body.username}';`
                , { nest: true }
            )
            if (users.length == 0) {
                res.status(200).send({"error" : "Incorrect username or password"});
            }
            // re-hash the password from request body and compare with hashed password in the db using the salt
            const user = users[0];
            if (typeof user !== "undefined") {
                let match = await comparePasswords(req.body.password, user.password, user.salt);
                if (!match) {
                    res.status(200).send({"error" : "Incorrect username or password"});
                } else {
                    res.status(200).send({"session_id": user.uuid, "user_id": users.id})
                }
            } else {
                res.status(200).send({"error" : "Incorrect username or password"});
            }
            
        } catch (err) {
            console.log(err);
            res.status(200).send({"error" : "Internal Server Error"});
        }
    });


    // the req.body.password is not delimitered; need to add single quotes
    // just changed
    app.post("/newuser", async (req, res) => {
        try {
            const uuid = uuidv4();
            // generate a random salt
            const salt = generateSalt(10);
            // generate a hashed password using the salt
            const password = await generateHash(req.body.password, salt);
            const user = await db.query(
                `INSERT INTO user_profile (username, password, salt, real_name, is_admin, uuid)
                Values
                ('${req.body.username}', '${password}', '${salt}', '${req.body.name}', false, '${uuid}');`
            )
            
            console.log(user);
            res.status(200).send({"session_id": uuid})
        } catch (err) {
            console.log(err);
            res.status(200).send({"error" : "Internal Server Error"});
        }
    });

    app.post("/validate",  async(req, res) => {
        try {
            const users = await db.query(
                `SELECT * FROM user_profile
                WHERE uuid = '${req.body.uuid}'`
                , { nest: true }
            )
            if (users.length == 0) {
                res.status(200).send({"error" : "User not found"});
            } else {
                res.status(200).send({"session_id": users[0].uuid, "user_id": users[0].id, 
                "user_name": users[0].username, "real_name": users[0].real_name})
            }
        } catch(err){
            console.log(err);
            res.status(200).send({"error" : "Internal Server Error"});
        }
    });

    app.get("/usersearch", async(req, res) => {
        try {
            const users = await db.query(
                `SELECT id, username FROM user_profile
                WHERE username LIKE '%${req.query.username}%'`
                , { nest: true }
            )
            res.status(200).send({"users": users})
        } catch (err) {
            console.log(err)
            res.status(200).send({"error" : "Internal Server Error"});
        }
    });

    return app;
}



