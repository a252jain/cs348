
const validateSession = async (db, user_id, session_id) =>  {
    const users = await db.query(
        `SELECT id, username FROM user_profile
        WHERE uuid = ${session_id}`
        , { nest: true }
    )
    console.log(users);
    if (users.length == 0) {
        throw new Error('Invalid session id passed');
    }
    if (users[0].id != user_id) {
        throw new Error('User is unauthorized to perform action');
    }
};

module.exports = {validateSession};
