const express = require("express");

module.exports = function(db) {
    const app = express();

    app.get("/playersearch", async (req, res) => {
        console.log(req);
        const page_number = ! req.query.page_number ? 0 : req.query.page_number;
        const count = ! req.query.count ? 10 : req.query.count;
        const player_name_filter = ! req.query.player_name  ? 'TRUE': `name like '%${req.query.player_name}%'`;
        const team_name_filter = ! req.query.team_name  ? 'TRUE' : `(t.short_name LIKE '%${req.query.team_name}%' OR t.long_name LIKE '%${req.query.team_name}%')`;
        const overall_rating_filter = ! req.query.overall_rating  ? 'TRUE': `overall_rating >= ${req.query.overall_rating}`;
        const potential_filter = ! req.query.potential  ? 'TRUE' : `potential >= ${req.query.potential}`;
        try {
            const players = await db.query(
                `SELECT p.id as pid, name, p.date as pdate, p.overall_rating as rating, p.potential as potential, t.long_name as team_name, t.team_id as tid
                FROM 
                    player p
                    LEFT JOIN most_recent_player_team_relation pr
                    ON p.id = pr.player_id
                    LEFT JOIN full_existing_team t
                    ON pr.team_id = t.team_id
                WHERE 
                   ${player_name_filter} AND 
                   ${team_name_filter} AND 
                   ${overall_rating_filter} AND 
                   ${potential_filter}
                ORDER BY overall_rating, potential DESC
                LIMIT ${page_number  * count}, ${count};`
                , { nest: true }
            )
            res.status(200).send({"players": players, "user_id": req.query.user_id});
        } catch (err) {
            res.status(200).send({"error" : "Internal Server Error"});
        }
    });

    return app;
}