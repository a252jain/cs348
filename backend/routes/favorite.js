const express = require("express");
const validator = require("./sessionValidator")

module.exports = function(db) {
    const app = express();

    app.post("/addfavorite", async (req, res) => {
        try {
            // await validator.validateSession(db, req.body.user_id, req.body.session_id);
            await db.query(
                `INSERT INTO favourite_relation (user_profile_id, team_id)
                Values (${req.body.user_id}, ${req.body.team_id});`
            )
            res.status(200).send({"user_id": req.body.user_id})
        } catch (err) {
            console.log(err);
            res.status(200).send({"error" : "Internal Server Error"});
        }
    });

    app.delete("/removefavorite", async (req, res) => {
        try {
            await db.query(
                `DELETE FROM favourite_relation WHERE user_profile_id = ${req.body.user_id} and team_id = ${req.body.team_id};`
            )
            res.status(200).send({"user_id": req.body.user_id})
        } catch (err) {
            res.status(200).send({"error" : "Internal Server Error"});
        }
    });

    app.get("/getfavorites", async (req, res) => {
        try {
            const favs = await db.query(
                `SELECT t.id, t.league_name, c.name as country, t.long_name, t.short_name, t.build_up_play_speed, t.build_up_play_passing, t.build_up_play_dribbling FROM favourite_relation as f
                INNER JOIN full_existing_team as t ON t.team_id = f.team_id
                AND user_profile_id = ${req.query.user_id}
                LEFT JOIN league l on t.league_name = l.name
                LEFT JOIN country c on c.id = l.country_id ;`
            , {nest: true})
            res.status(200).send({"favorites": favs, "user_id": req.query.user_id})
        } catch (err) {
            res.status(200).send({"error" : "Internal Server Error"});
        }
    });

    app.get("/getfavoriteids", async (req, res) => {
        try {
            const ids = await db.query(
                `SELECT team_id FROM favourite_relation
                WHERE user_profile_id = ${req.query.user_id} ;`
                , {nest: true})
            res.status(200).send({"id": ids, "user_id": req.query.user_id})
        } catch (err) {
            res.status(200).send({"error" : "Internal Server Error"});
        }
    });

    return app;
}