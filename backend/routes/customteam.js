const express = require("express");

module.exports = function(db) {
    const app = express();

    app.post("/addteam", async (req, res) => {
        try {
            const nt = await db.query(
                `CALL insert_custom_team('${req.body.long_name}', '${req.body.short_name}', '${req.body.league_name}', ${req.body.user_id}, ${req.body.score});`
            )
            console.log(nt);
            // console.log(nt[0]['0'].team_id)
            if (nt.length == 0) {
                throw new Error("insert failed");
            }
            res.status(200).send({"user_id": req.body.user_id, "team_id": nt[0].team_id})
        } catch (err) {
            res.status(200).send({"error" : "Internal Server Error"});
        }
    });

    app.delete("/removeteam", async (req, res) => {
        try {
            await db.query(
                `DELETE FROM custom_team WHERE team_id = ${req.body.team_id};`
            )
            res.status(200).send({"user_id": req.body.user_id})
        } catch (err) {
            res.status(200).send({"error" : "Internal Server Error"});
        }
    });

    app.post("/updateteam", async (req, res) => {
        try {
            await db.query(
                `Update custom_team SET score = ${req.body.score} WHERE team_id = ${req.body.team_id};
                Update team SET short_name = '${req.body.short_name}' WHERE id = ${req.body.team_id};
                Update team SET long_name = '${req.body.long_name}' WHERE id = ${req.body.team_id};`
            )
            res.status(200).send({"user_id": req.body.user_id})
        } catch (err) {
            res.status(200).send({"error" : "Internal Server Error"});
        }
    });

    app.post("/getteam", async (req, res) => {
        try {
            const team = await db.query(
                `SELECT player_id FROM player_in_custom_team
                WHERE team_id = ${req.body.team_id};`
            )
            res.status(200).send({"team": team, "user_id": req.body.user_id})
        } catch (err) {
            res.status(200).send({"error" : "Internal Server Error"});
        }
    });
    
    app.post("/addplayertoteam", async (req, res) => {
        try {
            await db.query(
                `INSERT INTO player_in_custom_team(player_id, team_id)
                Values(${req.body.player_id}, ${req.body.team_id});`
            )
            res.status(200).send({"user_id": req.body.user_id})
        } catch (err) {
            res.status(200).send({"error" : "Internal Server Error"});
        }
    });

    app.post("/getallcustomteams", async (req, res) => {
        try {
            const teams = await db.query(
                `SELECT team_id FROM custom_team
                WHERE user_profile_id = ${req.body.user_id};`
                , {nest: true}
            )
            res.status(200).send({"teams": teams})
        } catch (err) {
            res.status(200).send({"error" : "Internal Server Error"});
        }
    });

    return app;
}