const express = require("express");

module.exports = function(db) {
    const app = express();

    app.post("/addfriend", async (req, res) => {
        if (req.body.requester_id == req.body.requested_id) {
            res.status(200).send({"error": "Cannot follow yourself"})
            return;
        }
        try {
            await db.query(
                `INSERT INTO follows (follower_id, followee_id)
                Values (${req.body.requester_id}, ${req.body.requested_id});`
            )
            res.status(200).send({"user_id": req.body.requester_id})
        } catch (err) {
            res.status(200).send({"error" : "Internal Server Error"});
        }
    });

    app.delete("/removefriend", async (req, res) => {
        try {
            await db.query(
                `DELETE FROM follows
                WHERE follower_id = ${req.body.requester_id} and followee_id = ${req.body.requested_id};`
            )
            res.status(200).send({"user_id": req.body.user_id})
        } catch (err) {
            res.status(200).send({"error" : "Internal Server Error"});
        }
    });

    // gets all the users that are currently following the requested user
    app.get("/getfollowers", async (req, res) => {
        try {
            const followers = await db.query(
                `SELECT user_profile.username, user_profile.id 
                FROM 
                    follows
                    INNER JOIN user_profile
                    ON user_profile.id = follows.follower_id
                WHERE 
                    followee_id = ${req.query.user_id};`
            , {nest:true})
            res.status(200).send({"followers": followers, "user_id": req.query.user_id})
        } catch (err) {
            res.status(200).send({"error" : "Internal Server Error"});
        }
    });

    // gets all the users that the requested user is following
    app.get("/getfollowees", async (req, res) => {
        try {
            const followees = await db.query(
                `SELECT user_profile.username, user_profile.id 
                FROM 
                    follows
                    INNER JOIN user_profile
                    ON user_profile.id = follows.followee_id
                WHERE 
                    follower_id = ${req.query.user_id};`
            , {nest:true})
            res.status(200).send({"followees": followees, "user_id": req.query.user_id})
        } catch (err) {
            res.status(200).send({"error" : "Internal Server Error"});
        }
    })

    return app;
}