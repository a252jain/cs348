const Sequelize = require("sequelize");

const connect = function() {
  const sequelize = new Sequelize({
    database: process.env.DB_NAME, 
    username: process.env.DB_USER, 
    password: process.env.DB_PASSWORD,
    host: process.env.DB_HOST,
    dialect: "mysql",
    operatorsAliases: false
  })
  return sequelize;
}

const sequelize = connect();

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

module.exports = db;