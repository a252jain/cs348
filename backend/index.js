require("dotenv").config();
const express = require("express");
//const cors = require("cors");

const app = express();

/*var corsOptions = {
  origin: "http://localhost:8080"
};*/

//app.use(cors(corsOptions));
app.use(express.json());
//app.use(express.urlencoded({ extended: true }));

const db = require("./db.js");

function sync() {
  db.sequelize.sync().catch(() => {
    sync()
  })
  // add table creation here
}

sync();

const users = require("./routes/users")(db.sequelize);
const favorites = require("./routes/favorite")(db.sequelize);
const friends = require("./routes/friends")(db.sequelize);
const matchsearch = require("./routes/matchsearch")(db.sequelize);
const playersearch = require("./routes/playersearch")(db.sequelize);
const teamsearch = require("./routes/teamsearch")(db.sequelize);
const customteam = require("./routes/customteam")(db.sequelize);

app.use(users);
app.use(favorites);
app.use(friends);
app.use(matchsearch);
app.use(playersearch);
app.use(teamsearch);
app.use(customteam);

app.get("/", (req, res) => {
  res.json({ message: "Hello World!" });
});

const PORT = process.env.NODE_DOCKER_PORT || 3001;

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});