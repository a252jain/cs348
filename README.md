# CS348 Project

# Final project Info
- The final project content is in main right now

# How to run the docker container
- cd into the repo, and type `docker-compose up -d`
- This will run the docker for frontend, backend, and the database
- To build, do `docker compose build`

# How to interact with the local db & How to create all the tables
- Running docker compose up will open up a MySQL backend container.
- Right after container creation, the `init.sql` file inside the `database/creation` folder is run automatically to create all the tables, procedures and views.
- We use DataGrip to connect to the container through port 8081:
    - ![image-2.png](./image-2.png)
    - username: root, password: password

# How to import the production data
- Right now we manually import the 13 tables with 13 csv files
- The production data is in the `database/prod` folder with file names corresponding to the table name.
- Here is the importing order:  country -> league -> team -> existing_team -> user_profile -> custom_team -> match -> player -> player_in_match -> player_in_existing_team -> favourite_relation -> follows
- We plan on automating the data importing procedure by putting automated scripts to import.

# How to obtain the production data
- Our raw data comes from kaggle at https://www.kaggle.com/datasets/hugomathien/soccer
- It is an SQLite file, and processed using sql scripts inside the `database/processing` files.
- To obtain the prod data, open the sqlite filte, and run the files (step 1 - step 3 accordingly)
- the `SELECT` statements in step 3 are used to export the needed tables.
- THe processing was a bit complicated, so the production data is directly available in the `database/prod` folder.

# How to import sample data
- Use Datagrip's import functionality to import each table accordingly. -> use `database/sample_data`; importing order is same as production data specified above


# Features that the application supports
- Right now our frontend has all 7 features:
    - signup / login
    - search / filter players
    - search / filter match data
    - search / fillter team data
    - Following other users
    - Favoriting teams
    - Making custom teams
- Fancy feature
    - User session token
    - Password salting
    - Well designed UI
    - Complex, but minimal queries
    - Pagination
- Everything for the frontend is in the `/frontend` folder.
- Our database supports all the 7 basic features through local console.
- Our backend supports connection to the database + API hosting, and different kinds of sql queries / calling stored procedures through the APIs in the `backend/routes` folder.
