--- Export Data for the parent / abstract Team Table, based on the ER diagram required columns
SELECT id, team_long_name, team_short_name, Team_League_Relation.league_name FROM Team
LEFT JOIN Team_League_Relation on Team.team_api_id = Team_League_Relation.team_id;

-- Export data for the existing_team table
-- This is essentially the same as the Team_Attributes table from the raw data
SELECT Team.id as team_id, date, buildUpPlaySpeed, buildUpPlaySpeedClass, buildUpPlayDribbling, buildUPPlayDribblingClass, buildUpPlayPassing, buildUpPlayPassingClass,
       buildUpPlayPositioningClass, chanceCreationPassing, chanceCreationPassingClass, chanceCreationCrossing, chanceCreationCrossingClass,
       chanceCreationShooting, chanceCreationShootingClass, defencePressure, defencePressureClass, defenceAggression, defenceAggressionClass,
       defenceTeamWidth, defenceTeamWidthClass, defenceDefenderLineClass
FROM Team
LEFT JOIN Team_Attributes  on Team_Attributes.team_api_id = Team.team_api_id;

-- Export Player Data
--- Join Player and Player Attribute together, and export the data
SELECT p.id, player_name as name, date, overall_rating, potential, preferred_foot, attacking_work_rate, defensive_work_rate, crossing, finishing,
       standing_tackle, gk_handling, gk_reflexes, birthday, heading_accuracy, short_passing, volleys, dribbling, curve, free_kick_accuracy,
       long_passing, ball_control, acceleration, sprint_speed, agility, reactions, sliding_tackle, gk_kicking, balance,shot_power,
       jumping, stamina, strength, long_shots, aggression, interceptions, positioning, vision, penalties, marking, gk_diving, gk_positioning
FROM Player as p
INNER JOIN Player_Attributes as pa on p.player_fifa_api_id = pa.player_fifa_api_id;


-- Export Match data
SELECT Match.id, League.name as league_name, season, date, match_api_id, home.id as home_team_id, home_team_goal, away.id as away_team_id,
       away_team_goal, goal, shoton, shotoff, foulcommit, card, "cross", corner, possession, B365A, B365D, B365H
FROM Match
INNER JOIN Team home on home_team_api_id = home.team_api_id
INNER JOIN Team away on away_team_api_id = away.team_api_id
LEFT JOIN League on league_id = League.id;

-- Export League information
SELECT name, country_id FROM League;

-- Other tables like country did not need to be processed, so are exported directly.
