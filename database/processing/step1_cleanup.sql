-- The raw player_attribute table contains player attributes from different years
-- We only want most recent player attribute.
-- In raw player_attribute table, a player is identified by its player_fifa_id, so we check duplicate by checking on fifa id
DELETE FROM Player_Attributes WHERE id IN
  (SELECT a1.id FROM Player_Attributes a1, Player_Attributes a2
    WHERE a1.date < a2.date
        AND a1.player_fifa_api_id = a2.player_fifa_api_id);


-- Similarly, entries in the Team table with identical team_fifa_api_id needs to be removed,
-- Since we want all the teams to be distinct.

-- First we delete from the team_attributes table
DELETE FROM Team_Attributes WHERE team_api_id IN (
    SELECT a.team_api_id FROM Team as a, Team AS b WHERE a.id < b.id AND a.team_fifa_api_id = b.team_fifa_api_id
);

-- Then we delete from the Team table
DELETE FROM Team WHERE id IN (
    SELECT a.id FROM Team as a, Team AS b WHERE a.id < b.id AND a.team_fifa_api_id = b.team_fifa_api_id
);


-- A team can have different attributes from different dates,
-- We want to keep only the most recent attribute about a team
DELETE FROM Team_Attributes WHERE id IN
    (SELECT a1.id FROM Team_Attributes a1, Team_Attributes a2
        WHERE a1.date < a2.date
            AND a1.team_fifa_api_id = a2.team_fifa_api_id);


-- After the above query, we are supposed to have 1 to 1 mapping between the Player and Player_Attribute Table.
-- However there are some invalid entries, causing the same player_api_id to appear twice.
-- We got their id, 51009 and 141161
select * from Player_Attributes group by player_api_id having count(*) > 1;

-- Delete invalid data manually
DELETE FROM Player_Attributes where id = 51009 or id = 141161;


-- There are also some more incorrect data in player_attribute table.
-- After all the processing from above, the player_api_id and player_fifa_api_id
-- between Player and Player_Attributes should both match each other. HOwever, there are some
-- invalid data where the 2 id does not match between the 2 tables.
DELETE FROM Player_Attributes where  id in (
select pa.id from Player p
INNER JOIN Player_Attributes pa on pa.player_api_id = p.player_api_id
WHERE p.player_fifa_api_id <> pa.player_fifa_api_id);


-- We only want to select matches with full info on all the home and away players
DELETE FROM Match WHERE id NOT IN
    (SELECT id FROM Match WHERE
        home_player_1 is not null
        and home_player_2 is not null
        and home_player_3 is not null
        and home_player_4 is not null
        and home_player_5 is not null
        and home_player_6 is not null
        and home_player_7 is not null
        and home_player_8 is not null
        and home_player_9 is not null
        and home_player_10 is not null
        and home_player_11 is not null
        and away_player_1 is not null
        and away_player_2 is not null
        and away_player_3 is not null
        and away_player_4 is not null
        and away_player_5 is not null
        and away_player_6 is not null
        and away_player_7 is not null
        and away_player_8 is not null
        and away_player_9 is not null
        and away_player_10 is not null
        and away_player_11 is not null);