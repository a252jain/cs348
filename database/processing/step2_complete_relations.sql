-- there are no direct information on which team a player is in
-- the only palce where we can get those data is through the Match table, which stores the teams and players who played
-- First we create a temporary table for data processing.
CREATE TABLE Player_In_Team_Temp (
    id INTEGER PRIMARY KEY autoincrement,
    player_api_id INT,
    team_api_id INT,
    season VARCHAR(255),
    player_id INT,
    team_id INT
);

-- We gather all the player data based on the matches, and remove duplicates at the same time
-- UNION automatically removes duplicates.
-- We are basically registering relationship between the player's api_id and team's api_id, as well as the season
-- Season is important since a player can be in different team in different seasons / years.
INSERT INTO Player_In_Team_Temp (player_api_id, team_api_id, season)
SELECT home_player_1, home_team_api_id, season FROM Match
UNION
SELECT home_player_2, home_team_api_id, season FROM Match
UNION
SELECT home_player_3, home_team_api_id, season FROM Match
UNION
SELECT home_player_4, home_team_api_id, season FROM Match
UNION
SELECT home_player_5, home_team_api_id, season FROM Match
UNION
SELECT home_player_6, home_team_api_id, season FROM Match
UNION
SELECT home_player_7, home_team_api_id, season FROM Match
UNION
SELECT home_player_8, home_team_api_id, season FROM Match
UNION
SELECT home_player_9, home_team_api_id, season FROM Match
UNION
SELECT home_player_10, home_team_api_id, season FROM Match
UNION
SELECT home_player_11, home_team_api_id, season FROM Match
UNION
SELECT away_player_1, away_team_api_id, season FROM Match
UNION
SELECT away_player_2, away_team_api_id, season FROM Match
UNION
SELECT away_player_3, away_team_api_id, season FROM Match
UNION
SELECT away_player_4, away_team_api_id, season FROM Match
UNION
SELECT away_player_5, away_team_api_id, season FROM Match
UNION
SELECT away_player_6, away_team_api_id, season FROM Match
UNION
SELECT away_player_7, away_team_api_id, season FROM Match
UNION
SELECT away_player_8, away_team_api_id, season FROM Match
UNION
SELECT away_player_9, away_team_api_id, season FROM Match
UNION
SELECT away_player_10, away_team_api_id, season FROM Match
UNION
SELECT away_player_11, away_team_api_id, season FROM Match;


-- player_api_id is rather redundant -> we can connect directly with player id
UPDATE Player_In_Team_Temp
    SET player_id = player.id
    FROM (SELECT id, player_api_id FROM Player) as player
WHERE Player_In_Team_Temp.player_api_id = player.player_api_id;


-- Similarly, team_api_id is redundant -> we can directly connect with team id
UPDATE Player_In_Team_Temp
    SET team_id = team.id
    FROM (SELECT id, team_api_id FROM Team) as team
WHERE Player_In_Team_Temp.team_api_id = team.team_api_id;


-- Now we need to prepare for the addition of foreign keys

-- first, we validate foreign key constraint

-- this selection returned nothing, so the player constraint is functional
SELECT * FROM Player_In_Team_Temp WHERE player_api_id not in (
    select player_api_id from Player
    );

-- this selection returned nothing either, so the team constraint is functional
SELECT * FROM Player_In_Team_Temp WHERE team_api_id not in (
    select team_api_id from Team
    );


-- Add the final Player_In_Team table with foreign keys and uniqueness constraints
CREATE TABLE Player_In_Team (
    player_id INTEGER,
    team_id INTEGER,
    season VARCHAR(255),
    FOREIGN KEY (player_id) REFERENCES Player(id),
    FOREIGN KEY (team_id) REFERENCES Team(team_api_id),
    UNIQUE(player_id, team_id, season)
);

-- Transfer data from temp table to final table
INSERT INTO Player_In_Team (player_id, team_id, season)
SELECT player_id, team_id, season from Player_In_Team_Temp;

-- temporary player in match table to flatten match structure
CREATE TABLE player_in_match (
    player_api_id VARCHAR(255) NOT NULL,
    match_id INT NOT NULL,
    player_id VARCHAR(255),
    home_or_away VARCHAR(255),
    PRIMARY KEY (player_id, match_id, home_or_away),
    FOREIGN KEY (match_id) REFERENCES Match(id)
);

-- manually flatten the structure
INSERT INTO player_in_match (player_api_id, match_id,home_or_away)
select home_player_1, id, 'home' from Match
UNION
select home_player_2, id, 'home' from Match
UNION
select home_player_3, id, 'home' from Match
UNION
select home_player_4, id, 'home' from Match
UNION
select home_player_5, id, 'home' from Match
UNION
select home_player_6, id, 'home' from Match
UNION
select home_player_7, id, 'home' from Match
UNION
select home_player_8, id, 'home' from Match
UNION
select home_player_9, id, 'home' from Match
UNION
select home_player_10, id, 'home' from Match
UNION
select home_player_11, id, 'home' from Match
UNION
select away_player_1, id, 'away' from Match
UNION
select away_player_2, id, 'away' from Match
UNION
select away_player_3, id, 'away' from Match
UNION
select away_player_4, id, 'away' from Match
UNION
select away_player_5, id, 'away' from Match
UNION
select away_player_6, id, 'away' from Match
UNION
select away_player_7, id, 'away' from Match
UNION
select away_player_8, id, 'away' from Match
UNION
select away_player_9, id, 'away' from Match
UNION
select away_player_10, id, 'away' from Match
UNION
select away_player_11, id, 'away' from Match;

-- update player_id
UPDATE player_in_match
    SET player_id = player.id
    FROM (SELECT id, player_api_id FROM Player) as player
WHERE player_in_match.player_api_id = player.player_api_id;

-- final table to house player_in_match data
-- and checks for constraints
CREATE TABLE player_in_match_v2 (
    player_id INT NOT NULL,
    match_id INT NOT NULL,
    home_or_away VARCHAR(255) NOT NULL,
    PRIMARY KEY (player_id, match_id, home_or_away),
    FOREIGN KEY (player_id) REFERENCES Player(id),
    FOREIGN KEY (match_id) REFERENCES Match(id)
);

-- insert the data in
INSERT INTO player_in_match_v2
SELECT player_id, match_id, home_or_away FROM player_in_match;
