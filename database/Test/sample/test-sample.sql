-- **** The tests are performed on the production dataset, since it simulates
-- the final production environment + application better, and also because we
-- already have the production data ready.

-- -------------------------- Feature 1 tests

-- test 1 a), testing for people with name like aaron
SELECT p.id, name, p.date, p.overall_rating FROM player p
    LEFT JOIN most_recent_player_team_relation pr
    ON p.id = pr.player_id
    LEFT JOIN full_existing_team t
    ON pr.team_id = t.team_id
WHERE name like '%aaron%' AND
overall_rating >= 65
ORDER BY overall_rating, potential DESC
LIMIT 0, 50;

-- test 1 b), testing with more specific filters on team, potential, rating
SELECT p.id, name, p.date FROM player p
    LEFT JOIN most_recent_player_team_relation pr
    ON p.id = pr.player_id
    LEFT JOIN full_existing_team t
    ON pr.team_id = t.team_id
WHERE name like '%aaron doran%' AND
(t.short_name LIKE '%ry%' OR t.long_name LIKE '%r%') AND
overall_rating >= 65 AND
potential >= 65
ORDER BY overall_rating, potential DESC
LIMIT 0, 50;


-- ------------------------- Feature 2 tests

-- test 2 a), detailed search + filter by build up play speed
SELECT t.id, t.league_name,c.name FROM full_existing_team t
    LEFT JOIN league l on t.league_name = l.name
    LEFT JOIN country c on c.id = l.country_id
    WHERE (t.long_name LIKE '%rys%' OR t.short_name like '%rys%')
    AND t.league_name like '%England%'
    AND build_up_play_speed >= 30
    ORDER BY Build_up_play_speed DESC;

-- test 2 b), filtering by some attributes of the team
SELECT t.id, t.league_name,c.name FROM full_existing_team t
    LEFT JOIN league l on t.league_name = l.name
    LEFT JOIN country c on c.id = l.country_id
    WHERE build_up_play_passing >= 50
    AND build_up_play_speed >= 30
    AND build_up_play_dribbling >= 50
    ORDER BY Build_up_play_speed DESC;


-- ------------------------ Feature 3 tests

-- test 3 a), get total goals > 5
SELECT m.id, home_team_goal, away_team_goal FROM `match` m
    lEFT JOIN team t1
    ON t1.id = home_team_id
    LEFT JOIN team t2
    ON t2.id = away_team_id
    WHERE (away_team_goal + home_team_goal > 5)
    ORDER BY date;

-- test 3 b) search by team names
 SELECT * FROM `match` m
    lEFT JOIN team t1
    ON t1.id = home_team_id
    LEFT JOIN team t2
    ON t2.id = away_team_id
    WHERE (t1.long_name LIKE '%rys%' OR t1.short_name LIKE '%rys%')
    AND (t2.long_name LIKE '%pool%' OR t2.short_name LIKE '%pool%')
    ORDER BY date;



--  -------------------------- Feature 4 tests

-- 4 a) Inserting users into the database
INSERT INTO user_profile (username, password, real_name, email, is_admin)
Values
('jason1234', AES_ENCRYPT('xyz123hehe', 'secret'), 'Jason Zhao', 'abdcdd@gmail.com', false);

INSERT INTO user_profile (username, password, real_name, email, is_admin)
Values
('jason12344', AES_ENCRYPT('xyz123hehe', 'secret'), 'Jason Zhao', 'abcdcdd@gmail.com', false);

-- 4 b) Find user by username and password
SELECT * FROM user_profile
WHERE username = 'jason1234' AND AES_ENCRYPT('xyz123hehe', 'secret') = password;

-- 4 c) Unable to find the matching username and password
SELECT * FROM user_profile
WHERE username = 'jason1234' AND AES_ENCRYPT('xyz123hehe123', 'secret') = password;


-- --------------------- Feature 5 tests

-- 5 a) Insert info favourite_relation
INSERT INTO favourite_relation (user_profile_id, team_id)
Values (2, 12), (1,15), (2, 13);


-- 5 b) Delete a favourite_relation
DELETE FROM favourite_relation WHERE user_profile_id = 2 and team_id = 12;

-- 5 c) Get all favourite teams from a user
SELECT * FROM favourite_relation
INNER JOIN user_profile on user_profile_id = 2;

-- 5 d) Inserting a duplicate value
INSERT INTO favourite_relation (user_profile_id, team_id) Values (2, 13);


-- --------------------- Feature 6 tests

-- 6 a) Insert into custom team
INSERT INTO team (id, long_name, short_name, league_name)
Values (100001, 'jdjd', 'jdg', 'Belgium Jupiler League');
INSERT INTO custom_team
Values(100001, 2, 34);

-- 6 b) delete a custom team
DELETE FROM custom_team WHERE team_id = 100001;
DELETE FROM team WHERE id = 100001;

-- 6 c) update custom_team
Update custom_team SET score = 50 WHERE team_id = 100001;
Update team SET short_name = 'TEA' WHERE id = 100001;

-- 6 d) use a sql function for easier insertion
CALL insert_custom_team('test', 'ts', 'Belgium Jupiler League', 2, 34);

-- 6 e) add a player to a team
INSERT INTO player_in_custom_team(player_id, team_id)
Values(3, 100001);

-- 6 f) get players in a custom team
SELECT player_id FROM player_in_custom_team
WHERE team_id = 100001;

-- 6 g) get players in existing team
SELECT player_id FROM player_in_existing_team
WHERE team_id = 100001;

-- ------------------ Feature 7 starts

-- 7 a) Inserting some follow relations
INSERT INTO follows (follower_id, followee_id)
Values (1, 2), (2,1);


-- 7 b) Try to make a user follow itself
INSERT INTO follows (follower_id, followee_id)
Values (1, 1);

-- 7 c) Insert a duplicate value
INSERT INTO follows (follower_id, followee_id)
Values (2, 1);

-- 7 d) delete a follow relation
DELETE FROM follows
WHERE follower_id = 1 and followee_id = 2;

-- 7 e) use join to get all the followers of a user
SELECT user_profile.real_name, user_profile.id FROM follows
INNER JOIN user_profile
ON user_profile.id = follows.followee_id
WHERE followee_id = 1;

