-- a duplicate of the index file in `creation` folder
-- just to showcase the prod database tuning

-- feature 1 indexes
CREATE INDEX player_name ON player(name);
CREATE INDEX player_rating ON player(overall_rating);
CREATE INDEX player_potential ON player(potential);
CREATE INDEX player_id ON player(id);
CREATE INDEX player_in_existing_team_id ON player_in_existing_team(player_id);
CREATE INDEX team_id ON existing_team(team_id);
CREATE INDEX age ON player(birthday);

-- feature 2 indexes
-- league and country are very small databases with only about 20 entries, so no need for index
CREATE INDEX team_long_name ON team(long_name);
CREATE INDEX team_short_name ON team(short_name);
-- team's league_name column already has a default index
CREATE INDEX team_passing ON existing_team(build_up_play_passing);
CREATE INDEX team_speed ON existing_team(build_up_play_speed);
CREATE INDEX team_dribble ON existing_team(build_up_play_dribbling);

-- feature 3 indexes
CREATE INDEX match_home_team_id ON `match`(home_team_id);
CREATE INDEX match_away_team_id ON `match`(away_team_id);
-- create a generated column on the sum?
CREATE INDEX team_id ON team(id);
CREATE INDEX p_id_pim ON player_in_match(player_id);
CREATE INDEX m_id_pim ON player_in_match(match_id);

-- create new column so we can apply index
ALTER TABLE `match` ADD COLUMN total_goal INT DEFAULT 0;
UPDATE `match` SET total_goal = home_team_goal + away_team_goal;

CREATE index tg_match ON `match`(total_goal);



-- feature 4 indexes
CREATE INDEX profile_username ON user_profile(username);
CREATE INDEX profile_password ON user_profile(password);

-- feature 5 indexes
-- already has built in index on if


-- feature 6 indexes
CREATE INDEX custom_team_id ON custom_team(team_id);
CREATE INDEX custom_profile ON custom_team(user_profile_id);

-- feature 7 indexes
-- already has built in indexes