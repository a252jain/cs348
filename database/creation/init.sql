drop database cs348;
CREATE DATABASE IF NOT EXISTS cs348;

USE cs348;

SET GLOBAL local_infile = true;

SHOW GLOBAL VARIABLES LIKE 'local_infile';

-- DROP PROCEDURE IF EXISTS insert_custom_team;
-- DROP TABLE IF EXISTS follows;
-- DROP TABLE IF EXISTS favourite_relation;
-- DROP TABLE IF EXISTS player_in_custom_team;
-- DROP TABLE IF EXISTS custom_team;
-- DROP TABLE IF EXISTS user_profile;

-- country
CREATE TABLE IF NOT EXISTS country (
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    PRIMARY KEY (id)
);

-- league
CREATE TABLE IF NOT EXISTS league (
    name VARCHAR(255) NOT NULL,
    country_id INT NOT NULL,
    FOREIGN KEY (country_id) REFERENCES country(id),
    PRIMARY KEY (name)
);

-- team
-- how to insert into team, and its children? https://stackoverflow.com/questions/72014360/postgresql-simultaneous-insert-into-parent-child-table
CREATE TABLE IF NOT EXISTS team (
    id INT NOT NULL AUTO_INCREMENT,
    long_name VARCHAR(255) NOT NULL,
    short_name VARCHAR(255) NOT NULL,
    league_name VARCHAR(255),
    PRIMARY KEY (id),
    FOREIGN KEY (league_name) REFERENCES league(name)
);

-- existing_team
CREATE TABLE IF NOT EXISTS existing_team (
    team_id INT NOT NULL,
    date datetime,
    build_up_play_speed INT,
    build_up_play_speed_class VARCHAR(255),
    build_up_play_dribbling INT,
    build_up_play_dribbling_class VARCHAR(255),
    build_up_play_passing INT,
    build_up_play_passing_class VARCHAR(255),
    build_up_play_positioning_class VARCHAR(255),
    chance_creation_passing INT,
    chance_creation_passing_class VARCHAR(255),
    chance_creation_crossing INT,
    chance_creation_crossing_class VARCHAR(255),
    chance_creation_shooting INT,
    chance_creation_shooting_class VARCHAR(255),
    defence_pressure INT,
    defence_pressure_class VARCHAR(255),
    defence_aggression INT,
    defence_aggression_class VARCHAR(255),
    defence_team_width INT,
    defence_team_width_class VARCHAR(255),
    defence_defender_line_class VARCHAR(255),
    PRIMARY KEY (team_id),
    FOREIGN KEY (team_id) REFERENCES team(id)
);

-- user profile
CREATE TABLE IF NOT EXISTS user_profile (
    id INT NOT NULL AUTO_INCREMENT,
    username VARCHAR(255) NOT NULL UNIQUE,
    -- need to hash password
    password TEXT NOT NULL,
    salt VARCHAR(255) NOT NULL,
    real_name VARCHAR(255),
    email VARCHAR(255) UNIQUE,
    is_admin BOOLEAN DEFAULT FALSE,
    uuid VARCHAR(255) NOT NULL UNIQUE,
    PRIMARY KEY (id)
);


-- custom team
CREATE TABLE IF NOT EXISTS custom_team (
    team_id INT NOT NULL,
    user_profile_id INT NOT NULL,
    score INT NOT NULL,
    PRIMARY KEY (team_id),
    FOREIGN KEY (team_id) REFERENCES team(id),
    FOREIGN KEY (user_profile_id) REFERENCES user_profile(id)
);

-- player
CREATE TABLE IF NOT EXISTS player (
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(255),
    date datetime,
    overall_rating INT,
    potential INT,
    preferred_foot VARCHAR(255),
    attacking_work_rate VARCHAR(255),
    defensive_work_rate VARCHAR(255),
    crossing INT,
    finishing INT,
    standing_tackle INT,
    gk_handling INT,
    gk_reflexes INT,
    birthday datetime,
    heading_accuracy INT,
    short_passing INT,
    volleys INT,
    dribbling INT,
    curve INT,
    free_kick_accuracy INT,
    long_passing INT,
    ball_control INT,
    acceleration INT,
    sprint_speed INT,
    agility INT,
    reactions INT,
    sliding_tackle INT,
    gk_kicking INT,
    balance INT,
    shot_power INT,
    jumping INT,
    stamina INT,
    strength INT,
    long_shots INT,
    aggression INT,
    interceptions INT,
    positioning INT,
    vision INT,
    penalties INT,
    marking INT,
    gk_diving INT,
    gk_positioning INT,
    PRIMARY KEY (id)
);


-- match
CREATE table IF NOT EXISTS `match` (
    id INT NOT NULL AUTO_INCREMENT,
    league_name VARCHAR(255) NOT NULL,
    season VARCHAR(255),
    date datetime,
    match_api_id INT,
    home_team_id INT,
    home_team_goal INT,
    away_team_id INT,
    away_team_goal INT,
    B365A DOUBLE PRECISION,
    B365D DOUBLE PRECISION,
    B365H DOUBLE PRECISION,
    PRIMARY KEY (id),
    FOREIGN KEY (league_name) REFERENCES league(name)
);

-- player in match relation
CREATE TABLE IF NOT EXISTS player_in_match (
    player_id INT NOT NULL,
    match_id INT NOT NULL,
    home_or_away VARCHAR(255) NOT NULL CHECK (home_or_away = 'home' OR home_or_away = 'away'),
    PRIMARY KEY (player_id, match_id, home_or_away),
    FOREIGN KEY (player_id) REFERENCES player(id),
    FOREIGN KEY (match_id) REFERENCES `match`(id)
);

-- player relationship with existing team
CREATE TABLE IF NOT EXISTS player_in_existing_team (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    player_id INT NOT NULL,
    team_id INT NOT NULL,
    season VARCHAR(255),
    FOREIGN KEY (player_id) REFERENCES player(id),
    FOREIGN KEY (team_id) REFERENCES existing_team(team_id)
);

-- CREATE UNIQUE INDEX p_e_t ON player_in_existing_team(player_id, team_id, season);

-- New table for relationships between players and custom teams
CREATE TABLE IF NOT EXISTS player_in_custom_team (
    player_id INT NOT NULL,
    team_id INT NOT NULL,
    FOREIGN KEY (player_id) REFERENCES player(id),
    FOREIGN KEY (team_id) REFERENCES custom_team(team_id)
);

-- CREATE UNIQUE INDEX p_c_t ON player_in_custom_team(player_id, team_id);

-- favourite team relation
CREATE TABLE IF NOT EXISTS favourite_relation (
    user_profile_id INT NOT NULL,
    team_id INT NOT NULL,
    FOREIGN KEY (user_profile_id) REFERENCES user_profile(id),
    FOREIGN KEY (team_id) REFERENCES existing_team(team_id)
);

-- CREATE UNIQUE INDEX fav_rel ON favourite_relation(user_profile_id, team_id);

-- follower follows followee
-- a user cannot follow themselves
CREATE TABLE IF NOT EXISTS follows (
    follower_id INT NOT NULL,
    followee_id INT NOT NULL,
    FOREIGN KEY (follower_id) REFERENCES user_profile(id),
    FOREIGN KEY (followee_id) REFERENCES user_profile(id),
    UNIQUE(follower_id, followee_id),
    CONSTRAINT not_equal CHECK (follower_id <> followee_id)
);

-- a helper view to get the player's most recent teams
CREATE OR REPLACE DEFINER = 'root' VIEW most_recent_player_team_relation AS
    SELECT * FROM player_in_existing_team
    WHERE id NOT IN
    (
        SELECT a.id FROM player_in_existing_team a, player_in_existing_team b
        WHERE a.season < b.season and a.player_id = b.player_id
    );

SELECT * FROM most_recent_player_team_relation;

-- helper view to get all information about existing_team
CREATE OR REPLACE DEFINER = 'root' VIEW full_existing_team AS
    SELECT * FROM existing_team et
    INNER JOIN team t
    ON et.team_id = t.id;

-- helper procedure to insert into custom team.
DROP PROCEDURE IF EXISTS insert_custom_team;

CREATE DEFINER = 'root' PROCEDURE insert_custom_team (
    IN long_name varchar(255), IN short_name varchar(255), IN league_name varchar(255), IN user_profile_id int, IN score int
) BEGIN DECLARE team_id INT default 0; INSERT INTO team (long_name, short_name, league_name) VALUES(long_name, short_name, league_name); SET team_id = LAST_INSERT_ID();INSERT INTO custom_team (team_id, user_profile_id, score) VALUES(team_id, user_profile_id, score); SELECT team_id;END;


