/* Feature 1 Player Filter / Search
Query Template:
    SELECT * FROM Player p
        LEFT JOIN most_recent_player_team_relation pr
        ON p.id = pr.player_id
        LEFT JOIN full_existing_team t
        ON pr.team_id = t.team_id
    WHERE name like '%abc%' AND
    (t.short_name LIKE '%x%' OR t.long_name LIKE '%x%') AND
    overall_rating >= z AND
    potential >= y
    SORT BY m, n DESC
    LIMIT p, q
 */

SELECT * FROM player p
    LEFT JOIN most_recent_player_team_relation pr
    ON p.id = pr.player_id
    LEFT JOIN full_existing_team t
    ON pr.team_id = t.team_id
WHERE name like '%aron%' AND
(t.short_name LIKE '%rys%' OR t.long_name LIKE '%rys%') AND
overall_rating >= 65 AND
potential >= 65
ORDER BY overall_rating, potential DESC
LIMIT 0, 50;


/* Feature 2 Team Filter / Search
Search by team name, filter by league name
Query Template:
   SELECT * FROM full_existing_team t
    LEFT JOIN league l on t.league_name = l.name
    LEFT JOIN country c on c.id = l.country_id
    WHERE (t.long_name LIKE '%X%' OR t.short_name like '%Y%')
    AND t.league_name LIKE '%Z%'
    AND c.name = R
    AND build_up_play_speed >= M
    ORDER BY D DESC;
 */
SELECT * FROM full_existing_team t
    LEFT JOIN league l on t.league_name = l.name
    LEFT JOIN country c on c.id = l.country_id
    WHERE (t.long_name LIKE '%%' OR t.short_name like '%%')
    AND t.league_name LIKE '%%'
    AND c.name = 'France'
    AND build_up_play_speed >= 30
    ORDER BY Build_up_play_speed DESC;



/* Feature 3 Match Filter / Search
Query Template:
    SELECT * FROM `match` m
    lEFT JOIN team t1
    ON t1.id = home_team_id
    LEFT JOIN team t2
    ON t2.id = away_team_id
    WHERE (t1.long_name LIKE '%X%' OR t1.short_name LIKE '%Y%')
    AND (t2.long_name LIKE '%Z%' OR t2.short_name LIKE '%A%')
    AND (league_name LIKE '%B%')
    AND (away_team_goal + home_team_goal > C)
    ORDER BY R;
 */

 SELECT * FROM `match` m
    lEFT JOIN team t1
    ON t1.id = home_team_id
    LEFT JOIN team t2
    ON t2.id = away_team_id
    WHERE (t1.long_name LIKE '%%' OR t1.short_name LIKE '%%')
    AND (t2.long_name LIKE '%%' OR t2.short_name LIKE '%%')
    AND (league_name LIKE '%%')
    AND (away_team_goal + home_team_goal > 5)
    ORDER BY date;


/*
 Feature 4: User Signup / Log in
 Query template:
    INSERT:
         INSERT INTO user_profile (username, password, real_name, email, is_admin)
         Values (A, AES_ENCRYPT(B, 'secret'), C, D)
    
    SELECT:
        SELECT * FROM user_profile WHERE username = A AND AES_ENCRYPT(B, 'secret') = C
 */


 INSERT INTO user_profile (username, password, real_name, email, is_admin)
 Values
('jason1234', AES_ENCRYPT('xyz123hehe', 'secret'), 'Jason Zhao', 'abdcdd@gmail.com', false);

 INSERT INTO user_profile (username, password, real_name, email, is_admin)
 Values
('jason12344', AES_ENCRYPT('xyz123hehe', 'secret'), 'Jason Zhao', 'abcdcdd@gmail.com', false);

 SELECT * FROM user_profile
 WHERE username = 'jason1234' AND AES_ENCRYPT('xyz123hehe', 'secret') = password;


/*
 Feature 5: Favourite a team
 
 Query Template:
 
 INSERT INTO favourite_relation (user_profile_id, team_id)
    Values(A, B);
 
 DELETE FROM favourite_relation WHERE user_profile_id = A and team_id = B;
 */

 INSERT INTO favourite_relation (user_profile_id, team_id)
 Values (2, 12);

DELETE FROM favourite_relation WHERE user_profile_id = 2 and team_id = 12;

/*
 Feature 6: create custom team
 
 Query Template:
 
 INSERT:
     INSERT INTO Team (id, long_name, short_name, league_name)
        Values (A, B, C, D)
 
 Delete:
     DELETE FROM custom_team WHERE team_id = A
     DELETE FROM team WHERE id = A;
 
 Update:
    UPDATE custom_team
        SET long_name = A,
        short_name = B,
        league_name = C
    WHERE id = X;
 */

-- add a pre-insert constraint, or a view
INSERT INTO team (id, long_name, short_name, league_name)
Values (100001, 'jdjd', 'jdg', 'Belgium Jupiler League');
INSERT INTO custom_team
Values(100001, 2, 34);

-- delete custom team
DELETE FROM custom_team WHERE team_id = 100001;
DELETE FROM team WHERE id = 100001;


/*
 Feature 7: User a follows user b
 
 INSERT:
     INSERT INTO follows (follower_id, followee_id)
        Values (A, B), (C , D);
 
 DELETE:
    DELETE FROM follows
        WHERE follower_id = A and followee_id = B;
 
 */
INSERT INTO follows (follower_id, followee_id)
Values (1, 2), (2,1);

DELETE FROM follows
WHERE follower_id = 1 and followee_id = 2;
